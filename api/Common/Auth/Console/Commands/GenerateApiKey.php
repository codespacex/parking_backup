<?php namespace Api\Common\Auth\Console\Commands;

/**
 * File GenerateApiKeys.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Console\Commands
 * @subpackage GenerateApiKeys.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Illuminate\Console\Command;
use Api\Common\Auth\Models\ApiKey;

/**
 * Class GenerateApiKeys
 *
 * Command Class to create new Api Key
 *
 * @package    Api\Common\Auth\Console\Commands;
 * @subpackage GenerateApiKeys
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GenerateApiKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api-key:generate
                            {--id= : ID of the model you want to bind to this API key}
                            {--type= : The class name of the model you want to bind to this API key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an API key';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apiKeyableId    = $this->option('id');
        $apiKeyableModel = $this->option('type');
        $apiKey          = new ApiKey([
            'key'             => ApiKey::generateKey(),
            'apikeyable_id'   => $apiKeyableId,
            'apikeyable_type' => $apiKeyableModel,
        ]);
        $apiKey->save();
        $this->info('Generated API Key: ' . $apiKey->key);

        return;
    }
}