<?php namespace Api\Common\Auth\Http\Middleware;

/**
 * File AuthenticateApiKey.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Http\Middleware
 * @subpackage AuthenticateApiKey.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\ApiKeyNotFoundException;
use Api\Common\Exceptions\Exception;
use Api\Common\Exceptions\InvalidApiKeyException;
use Api\Common\Response;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Api\Common\Auth\Events\ApiKeyAuthenticated;
use Api\Common\Auth\Models\ApiKey;


/**
 * Class AuthenticateApiKey
 *
 * Verify and Authenticate Api Key passed on header or querystring
 *
 * @package    Api\Common\Auth\Http\Middleware;
 * @subpackage AuthenticateApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class AuthenticateApiKey
{

    /**
     * Handle an incoming request.
     *
     * @param  Request     $request
     * @param Closure      $next
     * @param  string|null $guard
     *
     * @throws ApiKeyNotFoundException
     * @throws InvalidApiKeyException
     *
     * @return Closure|Response
     */
    public function handle(Request $request, Closure $next)
    {
        
        try
        {
            $apiKeyValueFromHeader = $request->header('Authorization');
            $apiKeyValueFromQuery  = $request->get('api_key');
            if (empty($apiKeyValueFromHeader) && empty($apiKeyValueFromQuery))
            {
                throw new ApiKeyNotFoundException("API Key Not Found"); // API Key not provided
            }

            // Get API_KEY from header
            $apiKeyFromHeader = null;
            if (!empty($apiKeyValueFromHeader))
            {
                $bearer           = explode(' ', $apiKeyValueFromHeader);
                $apiKeyValue      = isset($bearer[1])?$bearer[1]:$bearer[0];
                $apiKeyFromHeader = $this->isApiKeyVerifiedFromHeader($apiKeyValue);
            }

            // Get API_KEY from QueryString
            if (empty($apiKeyFromHeader))
            {
                $apiKeyFromQuery = $this->isApiKeyVerifiedFromQuery($apiKeyValueFromQuery);
                if (empty($apiKeyFromQuery))
                {
                    throw new InvalidApiKeyException("Unauthorized Access!");
                }
                $apiKey = $apiKeyFromQuery;
            }
            else
            {
                $apiKey = $apiKeyFromHeader;
            }

            // Update this api key's last_used_at and last_ip_address
            $apiKey->update([
                'last_used_at'    => Carbon::now(),
                'last_ip_address' => $request->ip(),
            ]);

            $apikeyable = $apiKey->apikeyable;

            // Bind the user or object to the request
            // By doing this, we can now get the specified user through the request object in the controller using:
            // $request->user()
            $request->setUserResolver(function () use ($apikeyable) {
                return $apikeyable;
            });

            // Attach the apikey object to the request
            $request->apiKey = $apiKey;
            // Trigger event on successful API Authentication
            event(new ApiKeyAuthenticated($request, $apiKey));

            return $next($request);
        }
         catch (Exception $e)
        {
            $resp = new Response(null);

            return $resp->setErrorFromException(100, $e);
        }
    }

    /**
     * Verify Header Api Key
     *
     * @param string $apiKeyValue
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return string
     */
    public function isApiKeyVerifiedFromHeader($apiKeyValue)
    {
        $apiKey = app(ApiKey::class)->where('key', $apiKeyValue)->first();

        return $apiKey;
    }

    /**
     * Verify Api Key from QueryString
     *
     * @param string $apiKeyValue
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return ApiKey
     */
    public function isApiKeyVerifiedFromQuery($apiKeyValue)
    {
        $apiKey = app(ApiKey::class)->where('key', $apiKeyValue)->first();

        return $apiKey;
    }
}