<?php namespace Api\Common\Auth\Models;

/**
 * File ApiKeyable.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Models
 * @subpackage ApiKeyable.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */


/**
 * Class ApiKeyable
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKeyable
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
trait ApiKeyable
{
    /**
     * Get list of Api Key for given ID of Model
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function apiKeys()
    {
        return $this->morphMany(ApiKey::class, 'apikeyable');
    }

    /**
     * Create new Api Key for given model object
     *
     * @param $note
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return ApiKey
     */
    public function createApiKey($note = null)
    {
        return ApiKey::make($this , $note);
    }
}