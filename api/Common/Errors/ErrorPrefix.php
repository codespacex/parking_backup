<?php namespace Api\Common\Errors;

/**
 * File ErrorPrefix.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage ErrorPrefix.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class ErrorPrefix
 *
 * Define Prefix for Error Code
 *
 * @package    Api\Common\Errors;
 * @subpackage ErrorPrefix
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ErrorPrefix
{
    const SMART_MAILER = 10;
    const GENERAL = 20;
}