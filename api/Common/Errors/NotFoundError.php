<?php
namespace Api\Common\Errors;

/**
 * File NotFoundError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class NotFoundError
 *
 * Generate additional message when NotFoundException is thrown
 *
 * @package   Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class NotFoundError extends Error
{
    /**
     * @const int
     */
    const CODE = 404;

    /**
     * @const string
     */
    const MESSAGE = 'Records not found: ';
}