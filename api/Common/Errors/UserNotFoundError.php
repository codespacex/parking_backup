<?php
namespace Api\Common\Errors;

/**
 * File UserNotFoundError .php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class UserNotFoundError
 *
 * Generate additional message when NotFoundException is thrown
 *
 * @package   Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserNotFoundError extends Error
{
    /**
     * @const int
     */
    const CODE = 404;

    /**
     * @const string
     */
    const MESSAGE = 'User not found';
}