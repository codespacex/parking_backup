<?php namespace Api\Common\Exceptions;

/**
 * File CurlException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Exceptions
 * @subpackage Exception.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class CurlException
 *
 * Triggers when CURL error occurred
 *
 * @package    Api\Common\Exceptions;
 * @subpackage Exception
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CurlException extends Exception
{

}