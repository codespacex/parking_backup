<?php
namespace Api\Common\Exceptions;

/**
 * File InvalidIdException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Exceptions
 * @subpackage Exception.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidIdException
 *
 * Triggered for invalid request Id
 *
 * @package   Api\Common\Exceptions;
 * @subpackage Exception
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidIdException extends Exception
{

}