<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class AvailableRequests extends Model
{
    protected $fillable = [
        'user_id',
        'lng',
        'lat',
        'rating',
        'rating_count',
        'car_number',
        'color',
        'phone',
        'car_size'
    ];

}