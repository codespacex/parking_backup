<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class ButtonClick extends Model
{
    protected $fillable = [
        'user_id',
        'button_type'
    ];

}