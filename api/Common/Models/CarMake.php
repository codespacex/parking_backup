<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class CarMake extends Model
{
    protected $fillable = [
        'title',
        'sortorder',
    ];

}