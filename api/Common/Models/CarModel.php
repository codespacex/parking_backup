<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class CarModel extends Model
{
    protected $fillable = [
        'title',
        'car_make_id',
        'size'
    ];

}