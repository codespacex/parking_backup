<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class CodeMessage extends Model
{
    protected $fillable = [
        'code',
        'phone'
    ];

}