<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $fillable = [
        'contract_id',
        'comment'
    ];

}