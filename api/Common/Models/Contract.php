<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Contract extends Model
{
    protected $fillable = [
        'proposal_id',
        'status',
        'start',
        'rating',
        'feedback_id',
        'finish',
	'seller_id',
        'buyer_id',
	'street',
	'car_number',
	'is_cancelled'
    ];

}