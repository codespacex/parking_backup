<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Favourite extends Model
{
    protected $fillable = [
        'user_id',
        'lat',
        'lng',
	'street',
	'country'
    ];

}