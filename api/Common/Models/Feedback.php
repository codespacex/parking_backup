<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Feedback extends Model
{
    protected $fillable = [
        'content'
    ];

}