<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class ParkingProposal extends Model
{
    protected $fillable = [
        'buyer_id',
        'request_id',
        'status',
        'minutes',
        'seller_id',
        'lng',
        'lat'
    ];

}