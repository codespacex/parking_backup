<?php namespace Api\Common\Models;


use Illuminate\Database\Eloquent\Model;


class Transaction extends Model
{
    protected $fillable = [
        'phone',
        'user_id',
        'amount',
        'currency',
        'transaction_id',
        'type',
        'from_id',
        'to_id',
        'dt_time'
    ];

}