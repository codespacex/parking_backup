<?php namespace Api\Common;

/**
 * File Response.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common
 * @subpackage Response.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Errors\Error;
use Api\Common\Errors\OtherApplicationError;
use Api\Common\Exceptions\Exception;
use Api\Common\Helpers\HttpCode;

/**
 * Class Response
 *
 * @package    Api\V1\SmartMailer\Responses;
 * @subpackage Response
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class Response implements \JsonSerializable
{
    /**
     * @var mixed
     */
    private $payload;

    /**
     * @var mixed
     */
    private $pagination;

    /**
     * @var bool
     */
    private $success;

    /**
     * @var Error|null
     */
    private $error;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * @var \DateTime
     */
    private $statusCode;

    /**
     * @param            $payload
     * @param int        $statusCode
     * @param            $pagination
     * @param Error|null $error
     */
    public function __construct($payload = null, $statusCode = HttpCode::OK, $pagination = null, $error = null)
    {
        $this->payload    = $payload;
        $this->pagination = $pagination;
        $this->error      = $error;
        $this->success    = false;
        $this->statusCode = $statusCode;

        if ($error === null)
        {
            $this->success = true;
        }

        $this->dateTime = new \DateTime();
    }


    /**
     * Set success response data
     *
     * @param     $responseData
     *
     * @param int $statusCode
     *
     * @return Response
     */
    public function setSuccess($responseData, $statusCode = HttpCode::OK)
    {
        $this->payload    = $responseData;
        $this->pagination = null;
        $this->error      = null;
        $this->dateTime   = new \DateTime();
        $this->success    = true;
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Set success response data with pagination
     *
     * @param     $responseData
     * @param int $statusCode
     * @param     $pagination
     *
     * @return Response
     */
    public function setPaginatedSuccess($responseData, $pagination, $statusCode = HttpCode::OK)
    {
        $this->payload    = $responseData;
        $this->pagination = $pagination;
        $this->error      = null;
        $this->dateTime   = new \DateTime();
        $this->success    = true;
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     *  Set error response when error occurred
     *
     * @param            $errorCodePrefix
     * @param \Exception $exception
     *
     * @return Response
     */
    public function setErrorFromException($errorCodePrefix, \Exception $exception)
    {
        $exceptionClassName = get_class($exception);
        $errorName          = $this->getErrorNameFromExceptionName($exceptionClassName);

        $this->payload    = null;
        $this->pagination = null;
        $this->dateTime   = new \DateTime();
        $this->success    = false;

        if (class_exists($errorName) && $errorName !== Error::class)
        {
            $this->error = new $errorName(
                $errorCodePrefix, [
                $exception->getMessage(),
//                $exception->getFile(),
//                $exception->getLine(),
//                $exception->getTraceAsString(),
            ]);

            return $this;
        }

        $this->error = new OtherApplicationError(
            $errorCodePrefix, [
            $exception->getMessage(),
//            $exception->getFile(),
//            $exception->getLine(),
//            $exception->getTraceAsString(),
            $exceptionClassName,
        ]);

        return $this;
    }

    /**
     * Get Error from given Exception
     *
     * @param $exceptionName
     *
     * @return string
     */
    private function getErrorNameFromExceptionName($exceptionName)
    {
        $errorName = str_replace('Exception', 'Error', $exceptionName);

        return $errorName;
    }

    /**
     * Serialize the class properties into JSON
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return \stdClass
     */
    public function jsonSerialize()
    {
        $return          = new \stdClass();
        $return->success = $this->success;

        if ($this->payload !== null)
        {
            $return->payload = $this->payload;
        }

        if ($this->statusCode !== null)
        {
            $return->statusCode = $this->statusCode;
        }

        if ($this->error !== null)
        {
            $return->error = $this->error;
        }

        if ($this->pagination !== null)
        {
            $return->pagination = $this->pagination;
        }

        $return->dateTime = $this->dateTime;

        return $return;
    }

    /**
     * Get Data from the response
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return mixed|null
     */
    public function getContent()
    {
        return $this->payload;
    }

}