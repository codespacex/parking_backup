<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\BuyService;
use Api\V1\General\Services\SellService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class BuyController extends ApiController
{

    protected $buyService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->buyService = new BuyService();
    }

    public function find(Request $request)
    {
        try {
            $lists = $this->buyService->find($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function propose(Request $request)
    {
        try {
            $lists = $this->buyService->propose($request->all(),$request->id,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function cancelProposal(Request $request)
    {
        try {
            $lists = $this->buyService->cancelProposal($request->id,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function feedbackList(Request $request)
    {
        try {
            $lists = $this->buyService->feedbackList($request->all());
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function rate(Request $request)
    {
        try {
            $lists = $this->buyService->rate($request->all());
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function feedback(Request $request)
    {
        try {
            $lists = $this->buyService->feedback($request->all());
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function comment(Request $request)
    {
        try {
            $lists = $this->buyService->comment($request->all());
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function addToFavourite(Request $request)
    {
        try {
            $lists = $this->buyService->addToFavourite($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function deleteFavourite(Request $request)
    {
        try {
            $lists = $this->buyService->deleteFavourite($request->id,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function finishContract(Request $request)
    {
        try {
            $lists = $this->buyService->finishContract($request->id,$request->street,$request->isCancel,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function history(Request $request)
    {
        try {
            $lists = $this->buyService->history($request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }
	
    public function historySale(Request $request)
    {
        try {
            $lists = $this->buyService->historySale($request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }
	

    public function favourites(Request $request)
    {
        try {
            $lists = $this->buyService->favourites($request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }	
}