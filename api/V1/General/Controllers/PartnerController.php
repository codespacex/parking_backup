<?php
namespace Api\V1\General\Controllers;

/**
 * File PartnerController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage PartnerController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\ParthnerService;
use Api\V1\General\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class PartnerController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage PartnerController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class PartnerController extends ApiController
{

    protected $partnerService;

    /**
     * PartnerController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->partnerService = new ParthnerService();
    }

    public function check(Request $request)
    {
        try {
            $lists = $this->partnerService->check($request->all());
            return Response::json($lists);

        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function payment(Request $request)
    {
        try {
            $lists = $this->partnerService->payment($request->all());
            return Response::json($lists);

        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

}