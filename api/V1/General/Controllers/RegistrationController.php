<?php
namespace Api\V1\General\Controllers;


use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\RegisterService;

/**
 * Class RegistrationController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage RegistrationController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class RegistrationController extends ApiController
{

    protected $registerService;

    /**
     * RegistrationController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->registerService = new RegisterService();
    }

    public function register(Request $request){
        try {
            $sites = $this->registerService->register($request->all());
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function confirm(Request $request){
        try {
            $sites = $this->registerService->confirm($request->all());
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function finish(Request $request){
        try {
            $sites = $this->registerService->finish($request->all());
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }
    
    public function editPhone(Request $request){
        try {
            $sites = $this->registerService->editPhone($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function confirmEditPhone(Request $request){
        try {
            $sites = $this->registerService->confirmEditPhone($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function editUser(Request $request){
        try {
            $sites = $this->registerService->editUser($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($sites);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

}