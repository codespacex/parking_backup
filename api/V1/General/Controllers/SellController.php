<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\SellService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class SellController extends ApiController
{

    protected $sellService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->sellService = new SellService();
    }

    public function becomeAvailable(Request $request)
    {
        try {
            $lists = $this->sellService->becomeAvailable($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function stopAvailable(Request $request)
    {
        try {
            $lists = $this->sellService->stopAvailable($request->all(),$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function acceptProposal(Request $request)
    {
        try {
            $lists = $this->sellService->acceptProposal($request->id,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function declineProposal(Request $request)
    {
        try {
            $lists = $this->sellService->declineProposal($request->id,$request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function sendLocation(Request $request)
    {
        try {
            $lists = $this->sellService->sendLocation($request->all(),'0');
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {	
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }

}