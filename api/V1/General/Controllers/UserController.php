<?php
namespace Api\V1\General\Controllers;

/**
 * File SalonsController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage SalonsController.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class SalonsController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage SalonsController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserController extends ApiController
{

    protected $userService;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = new UserService();
    }

    public function update(Request $request)
    {
        try {
            if (!is_numeric($request->id)){
                throw new InvalidIdException();
            }
            $lists = $this->userService->update($request->all(),$request->id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }
    
    public function getUser(Request $request)
    {
        try {
            $lists = $this->userService->getUser($request->apiKey->apikeyable_id);
            $result     = $this->response->setSuccess($lists);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::FORBIDDEN;
            $result     = $this->response->setErrorFromException(ErrorPrefix::SMART_MAILER, $e);
        }

        return Response::json($result, $httpStatus);
    }


}