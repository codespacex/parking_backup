<?php
namespace Api\V1\General\Entities;

/**
 * File Entity.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Entities
 * @subpackage Entity.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */


/**
 * Class Entity
 *
 * @package    Api\V1\General\Entities;
 * @subpackage Entity
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class Entity
{

    protected function checkArrayIndex(array $array , $index){

        if (!isset($array[$index])){
            throw new \Exception("Array index ".$index." does not exist");
        }

        return $array[$index];
    }
}