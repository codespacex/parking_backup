<?php
namespace Api\V1\General\Entities;

/**
 * File UserEntity.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Entities
 * @subpackage UserEntity.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 * @copyright  2018 Kyvio.com All rights reserved.
 */

/**
 * Class UserEntity
 *
 * @package    Api\V1\General\Entities
 * @subpackage UserEntity
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class GoodEntity extends Entity implements \JsonSerializable
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $category_id;

    /**
     * @var string
     */
    private $user_id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $price;

    /**
     * @var int
     */
    private $description;

    /**
     * @var int
     */
    private $short_description;

    /**
     * @var int
     */
    private $photo;

    /**
     * @var int
     */
    private $photo_description;

    /**
     * @var int
     */
    private $category;

    /**
     * @var int
     */
    private $tags;

    /**
     * @var int
     */
    private $created_at;

    /**
     * UserEntity constructor.
     *
     * @param array $data
     *
     */
    public function __construct(array $data)
    {
        $this->goodId    = $this->checkArrayIndex($data , 'goodId');
        $this->category_id     = $this->checkArrayIndex($data , 'category_id');
        $this->category     = $this->checkArrayIndex($data , 'category');
        $this->tags     = $this->checkArrayIndex($data , 'tags');
        $this->user_id  = $this->checkArrayIndex($data , 'user_id');
        $this->title = $this->checkArrayIndex($data ,'title');
        $this->price  = $this->checkArrayIndex($data , 'price');
        $this->created_at = $this->checkArrayIndex($data , 'createdAt');
    }

    /**
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $return = $this;

        return get_object_vars($return);
    }


}