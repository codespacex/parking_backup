<?php
namespace Api\V1\General\Errors;

/**
 * File InvalidDataError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage InvalidDataError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidDataError
 *
 * @package   Api\V1\SmartMailer\Errors;
 * @subpackage InvalidDataError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidDataError extends ValidationError
{
    /**
     * error code
     */
    const CODE = 403;

    /**
     * error message
     */
    const MESSAGE = 'Invalid data';
}