<?php namespace Api\V1\General\Errors;

/**
 * File UserWithEmailAlreadyExistsError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage UserWithEmailAlreadyExistsError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class UserWithEmailAlreadyExistsError
 *
 * Generate additional message when UserWithEmailAlreadyExistsException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage UserWithEmailAlreadyExistsError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserWithEmailAlreadyExistsError extends ValidationError
{
    /**
     * @const int
     */
    const CODE = 1001;

    /**
     * @const string
     */
    const MESSAGE = 'Cannot Create User';
}