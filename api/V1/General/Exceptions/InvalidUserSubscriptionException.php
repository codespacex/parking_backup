<?php namespace Api\V1\General\Exceptions;

/**
 * File InvalidUserSubscriptionException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage InvalidUserSubscriptionException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class InvalidUserSubscriptionException
 *
 * Generated when User Registration is invalid
 *
 * @package    Api\V1\SmartMailer\Exceptions;
 * @subpackage InvalidUserSubscriptionException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidUserSubscriptionException extends Exception
{

}