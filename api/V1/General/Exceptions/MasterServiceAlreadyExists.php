<?php namespace Api\V1\General\Exceptions;

/**
 * File MasterServiceAlreadyExists.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Exceptions
 * @subpackage MasterServiceAlreadyExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class MasterServiceAlreadyExists
 *
 * Generated when User Registration is invalid
 *
 * @package    Api\V1\General\Exceptions;
 * @subpackage MasterServiceAlreadyExists
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class MasterServiceAlreadyExists extends Exception
{

}