<?php namespace Api\V1\General\Exceptions;

/**
 * File NoUserfoundWithUsernameException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage NoUserfoundWithUsernameException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class NoUserfoundWithUsernameException
 *
 * Generated when User cannot be created
 *
 * @package    Api\V1\SmartMailer\Exceptions;
 * @subpackage NoUserfoundWithUsernameException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class NoUserFoundWithUsernameException extends Exception
{

}