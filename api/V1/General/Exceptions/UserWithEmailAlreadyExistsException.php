<?php namespace Api\V1\General\Exceptions;

/**
 * File UserWithEmailAlreadyExistsException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage UserWithEmailAlreadyExistsException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class UserWithEmailAlreadyExistsException
 *
 * Generated when User already exists with given email
 *
 * @package    Api\V1\SmartMailer\Exceptions;
 * @subpackage UserWithEmailAlreadyExistsException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserWithEmailAlreadyExistsException extends Exception
{

}