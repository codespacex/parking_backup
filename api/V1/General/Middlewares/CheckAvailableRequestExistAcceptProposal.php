<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Response;
use Api\V1\General\Models\UParkingProposal;
use Api\V1\General\Models\UUser;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Api\Common\Exceptions\NotFoundException;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckAvailableRequestExistAcceptProposal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {
            $user = UUser::where('id',$request->apiKey->apikeyable_id)
                ->with('car')
                ->first();

            if(!$user){
                throw new NotFoundException('User not found.');
            }

            $proposal = UParkingProposal::where('id',$request->id)
                ->where('status','pending')
                ->with('request')
                ->first();

            if(!$proposal)throw new NotFoundException('Proposal not found.');

            if($proposal->seller_id != $request->apiKey->apikeyable_id)throw new NotFoundException('You do not have permission to accept this proposal.');


        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}