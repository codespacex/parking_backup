<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Response;
use Api\V1\General\Models\UAvailableRequests;
use Api\V1\General\Models\UParkingProposal;
use Api\V1\General\Models\UUser;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Api\Common\Exceptions\NotFoundException;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckAvailableRequestExistProposal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $user = UUser::where('id',$request->apiKey->apikeyable_id)
                ->with('car')
                ->first();

            if(!$user){
                throw new NotFoundException('User not found.');
            }
            $avail_request = UAvailableRequests::where('id',$request->id)
                ->with('user')
                ->first();

            if(!$avail_request)throw new NotFoundException('Request not found.');
            if($user->car->size > $avail_request->user->car->size)throw new NotFoundException('Your car size is smaller than requested car size.');

            $proposal = UParkingProposal::where('request_id',$request->id)
                                        ->where('buyer_id',$request->apiKey->apikeyable_id)
                                        ->first();
            if($proposal)throw new NotFoundException('You have already proposed to this request.');

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}