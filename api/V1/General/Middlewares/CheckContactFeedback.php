<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Response;
use Api\V1\General\Models\UCarModel;
use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UFeedback;
use Api\V1\User\Models\UFavourite;
use Api\V1\User\Models\UUser;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidDataException;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckContactFeedback
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $rules = [
                'feedback_id' => 'required|integer',
                'contract_id' => 'required|integer',
            ];

            $validator = Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                throw new InvalidDataException($validator->errors());
            }

            $feedback = UFeedback::find($request->feedback_id);
            if(!$feedback){
                throw new NotFoundException('Feedback not found.');
            }

            $contract = UContract::where('id',$request->contract_id)
                ->with('proposal')
                ->first();
            if(!$contract){
                throw new NotFoundException('Contract not found.');
            }

            if($request->apiKey->apikeyable_id != $contract->proposal->buyer_id){
                throw new NotFoundException("You don't have access to do that");
            }

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}