<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Response;
use Api\V1\General\Models\UCarModel;
use Api\V1\User\Models\UFavourite;
use Api\V1\User\Models\UUser;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidDataException;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckPhoneExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $rules = [
                'phone' => 'required|min:5',
//                'car_number' => 'required',
//                'car_id' => 'required',
                'name' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                throw new InvalidDataException($validator->errors());
            }

            $user = User::where('phone',$request->phone)
                            ->where('is_confirmed',1)
                            ->first();

            if($user){
                throw new NotFoundException();
            }

//            $car = UCarModel::find($request->car_id);
//            if(!$car){
//                throw new InvalidDataException("The model doesn't exist");
//            }


        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}