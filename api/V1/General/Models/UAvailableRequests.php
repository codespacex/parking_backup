<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\AvailableRequests;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UAvailableRequests extends AvailableRequests
{
    protected $table = 'available_requests';

    public function user() {
        return $this->hasOne(UUser::class , 'id','user_id')->with('car','proposals');
    }
}