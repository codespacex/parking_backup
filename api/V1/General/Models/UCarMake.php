<?php namespace Api\V1\General\Models;


/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\CarMake;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UCarMake extends CarMake
{
    protected $table = 'car_makes';

    public function models() {
        return $this->hasMany(UCarModel::class , 'car_make_id','id');
    }
}