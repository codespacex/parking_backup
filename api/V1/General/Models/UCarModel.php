<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\CarMake;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UCarModel extends CarMake
{
    protected $table = 'car_models';

    public function make() {
        return $this->hasOne(UCarMake::class , 'id','car_make_id');
    }
}