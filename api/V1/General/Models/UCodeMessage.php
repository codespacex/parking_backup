<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\CodeMessage;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UCodeMessage extends CodeMessage
{
    protected $table = 'code_messages';

    public static function generateCode($phone) {

        $code = random_int(1001,9999);

        UCodeMessage::create([
            'phone' => $phone,
            'code' => $code
        ]);

        return $code;
    }

}