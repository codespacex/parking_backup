<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\Contract;
use Api\Common\Models\ParkingProposal;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UContract extends Contract
{
    protected $table = 'contracts';

    public function proposal() {
        return $this->hasOne(UParkingProposal::class , 'id','proposal_id')->with('request','buyer');
    }
    
    public function prop() {
        return $this->hasOne(UParkingProposal::class , 'id','proposal_id');
    }
}