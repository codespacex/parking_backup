<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\Favourite;
use Api\Common\Models\Feedback;
use App\User;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UFavourite extends Favourite
{
    protected $table = 'favourites';
}