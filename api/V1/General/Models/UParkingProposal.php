<?php namespace Api\V1\General\Models;

/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Models\ParkingProposal;
use App\User;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UParkingProposal extends ParkingProposal
{
    protected $table = 'parking_proposals';

    public function request() {
        return $this->hasOne(UAvailableRequests::class , 'id','request_id')->with('user');
    }

    public function buyer() {
        return $this->hasOne(UUser::class , 'id','buyer_id')->with('car','bought');
    }

    public function seller() {
        return $this->hasOne(UUser::class , 'id','seller_id')->with('car','bought');
    }

    public function contract() {
        return $this->hasOne(UContract::class , 'proposal_id','id');
    }
}