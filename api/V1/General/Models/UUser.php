<?php namespace Api\V1\General\Models;


/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use App\User;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UUser extends User
{
    protected $table = 'users';
    
    public static function recalculateBalance($id){
        $user = self::find($id);
        $transactions = UTransaction::where('user_id',$id)->get();
        $balance = 0;

        foreach ($transactions as $transaction){
            if($transaction->type == 'payment'){
                $balance+= $transaction->amount;
            }
            if($transaction->type == 'cash'){
                $balance-= $transaction->amount;
            }
        }

        $user->balance = $balance;
        $user->save();
        return $balance;
    }

    public function car() {
        return $this->hasOne(UCarModel::class , 'id','car_id')->with('make');
    }

    public function bought(){
        return $this->hasMany(UParkingProposal::class , 'buyer_id','id')->with('contract');
    }
    
    public function proposals(){
        return $this->hasMany(UParkingProposal::class , 'buyer_id','id');
    }
    public function sold(){
        return $this->hasMany(UContract::class , 'seller_id','id');
    }	
}