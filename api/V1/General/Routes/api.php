<?php

use Api\V1\General\Middlewares\CheckPhoneExist;
use Api\V1\General\Middlewares\CheckPhoneExistLogin;
use Api\V1\General\Middlewares\CheckAvailableRequestExist;
use Api\V1\General\Middlewares\CheckAuthUsers;
use Api\V1\General\Middlewares\CheckAvailableRequestExistProposal;
use Api\V1\General\Middlewares\CheckAvailableRequestExistCancelProposal;
use Api\V1\General\Middlewares\CheckAvailableRequestExistAcceptProposal;
use Api\V1\General\Middlewares\CheckContractRate;
use Api\V1\General\Middlewares\CheckContactFeedback;
use Api\V1\General\Middlewares\CheckContactComment;
use Api\V1\General\Middlewares\CheckFavourite;
use Api\V1\General\Middlewares\CheckSendLocation;
use Api\V1\General\Middlewares\CheckFinish;
use Api\V1\General\Middlewares\CheckBuyerFinishContract;


Route::prefix('general')->group(function () {

    // Parthner commands
    Route::post('/Check', 'PartnerController@check');
    Route::post('/Payment', 'PartnerController@payment');

    //Gets list of Car Makes
    Route::get('/car-makes/list', 'CarMakesController@index');
    //Gets list of Feedbacks
    Route::get('/feedbacks/list', 'BuyController@feedbackList');
    //Gets list of prices
    Route::get('/prices/list', 'CarMakesController@prices');

    // Phone register and login
    Route::post('/register-phone' , 'RegistrationController@register')->middleware([CheckPhoneExist::class]);
    Route::post('/confirm-registration' , 'RegistrationController@confirm');

    // Phone register and login
    Route::post('/login-phone' , 'LoginController@login')->middleware([CheckPhoneExistLogin::class]);
    Route::post('/confirm-login' , 'LoginController@confirm');
    Route::post('/finish-registration' , 'RegistrationController@finish')->middleware([CheckFinish::class]);
    Route::post('/send-location' , 'SellController@sendLocation')->middleware([CheckSendLocation::class]);

    Route::middleware(['auth.apikey'])->group(function () {

	    // Edit user field
        Route::post('/edit-phone-user' , 'RegistrationController@editPhone');
        Route::post('/confirm-edit-phone-user' , 'RegistrationController@confirmEditPhone');
        Route::post('/edit-user' , 'RegistrationController@editUser');
        Route::put('/user/update/{id}' , 'UserController@update')->middleware([CheckAuthUsers::class]);

        //Logout
        Route::get('/logout' , 'LoginController@logout');
        //Get User command
	    Route::get('/get-user' , 'UserController@getUser');

        //Seller Commands
        Route::post('/sell/available-start' , 'SellController@becomeAvailable')->middleware([CheckAvailableRequestExist::class]);
        Route::get('/sell/available-stop' , 'SellController@stopAvailable');
        Route::get('/sell/accept-proposal/{id}' , 'SellController@acceptProposal')->middleware([CheckAvailableRequestExistAcceptProposal::class]);
        Route::get('/sell/decline-proposal/{id}' , 'SellController@declineProposal')->middleware([CheckAvailableRequestExistAcceptProposal::class]);

        //Buyer commands
        Route::get('/buy/find-available' , 'BuyController@find');
        Route::post('/buy/propose/{id}' , 'BuyController@propose')->middleware([CheckAvailableRequestExistProposal::class]);
        Route::get('/buy/cancel-proposal/{id}' , 'BuyController@cancelProposal')->middleware([CheckAvailableRequestExistCancelProposal::class]);
        Route::get('/buy/finish-contract/{id}/{street}/{isCancel}' , 'BuyController@finishContract')->middleware([CheckBuyerFinishContract::class]);
        Route::post('/buy/add-to-favourite' , 'BuyController@addToFavourite')->middleware([CheckFavourite::class]);
        Route::delete('/buy/delete-favourite/{id}' , 'BuyController@deleteFavourite');
        Route::post('/buy/rate' , 'BuyController@rate')->middleware([CheckContractRate::class]);
        Route::post('/buy/feedback' , 'BuyController@feedback')->middleware([CheckContactFeedback::class]);
        Route::post('/buy/comment' , 'BuyController@comment')->middleware([CheckContactComment::class]);

        
	    // User history command
        Route::get('/history/parking' , 'BuyController@history');
        Route::get('/history/sale' , 'BuyController@historySale');
        // Favourites command
        Route::get('/favourites' , 'BuyController@favourites');
    });
});
