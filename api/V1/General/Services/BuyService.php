<?php
namespace Api\V1\General\Services;

use Api\Common\Models\AvailableRequests;
use Api\V1\General\Models\UAvailableRequests;
use Api\V1\General\Models\UButtonClick;
use Api\V1\General\Models\UComment;
use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UFavourite;
use Api\V1\General\Models\UPrice;
use Api\V1\General\Models\UTransaction;
use Api\V1\General\Models\UFeedback;
use Api\V1\General\Models\UParkingProposal;
use Api\V1\General\Models\UUser;
use App\Events\ProposalCanceled;
use App\Events\ProposalProposed;
use App\Events\ProposalFinished;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Exceptions\Exception;
use Illuminate\Support\Facades\Validator;
use App\Events\RequestAvailableStoped;
use App\Events\RequestAvailableStarted;
use Api\Common\Auth\Models\ApiKey;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class BuyService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'minutes' => 'required|numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$available_request,$user_id)
    {
        return UParkingProposal::create([
            'minutes' => $data['minutes'],
            'request_id' => $available_request['id'],
            'buyer_id' => $user_id,
            'seller_id' => $available_request['user_id'],
            'lng' => $available_request['lng'],
            'lat' => $available_request['lat']
        ]);
    }

    public function find($request,$user_id)
    {
        try{
            // Creating click.
            UButtonClick::create([
                'user_id' => $user_id,
                'button_type' => 'find'
            ]);

            $user = UUser::where('id',$user_id)->with('car')->first();
            if(!$user || empty($user->car)) throw new NotFoundException('User not found.');
            $requests = UAvailableRequests::where('car_size', '>=',$user->car->size)->get();
            return $requests;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function cancelProposal($proposal_id, $buyer_id)
    {
        try{
            $proposal = UParkingProposal::where('id',$proposal_id)->where('buyer_id',$buyer_id)->where('status','pending')->first();
            if(!$proposal)throw new NotFoundException('Proposal not found.');
            $proposal->status = 'declined';
            $proposal->save();
	    
	        $user = UUser::where('id', $proposal->seller_id)->with('car','sold')->first();

            $availableDec = AvailableRequests::create([
                'user_id' => $proposal->seller_id,
                'lng' => $proposal->lng,
                'lat' => $proposal->lat,
                'rating_count' => count($user->sold),
                'car_number' => $user->car_number,
                'color' => $user->color,
                'phone' => $user->phone,
                'car_size' => $user->car->size
            ]);
	        $new = $availableDec->toArray();

	        $tokens_available = ApiKey::where('car_size','<=',$user->car->size)->where('apikeyable_id','!=',$proposal->seller_id)->pluck('key')->toArray();
	
	        event(new RequestAvailableStarted(['data'=>$new,'token' => $tokens_available]));
	    
	        $tokens = [];
	        $api_key = 	ApiKey::where('apikeyable_id',$proposal->seller_id)->first();
	        if($api_key){
		    $key = $api_key->key;
		        array_push($tokens, $key);
	        }else{
		        array_push($tokens, 'not');
	        }

            event(new ProposalCanceled(['data'=>$proposal,'token' => $tokens]));

            return true;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function propose($request,$request_id, $buyer_id)
    {
        try{
            if( count($this->validator($request)->messages()) ){
                throw new NotFoundException($this->validator($request)->messages());
            }else{
                try{
                    $available_request = UAvailableRequests::where('id',$request_id)->first();
                    if(!$available_request)throw new NotFoundException('Available request not found');
		            $data = [];
		            $data['id'] = $available_request->id;
		            $data['buyer_id'] = $buyer_id;

		            $balance = UUser::recalculateBalance($buyer_id);
		            $price = UPrice::where('key','find_price')->first();
		            if($price->price > $balance){
			            throw new NotFoundException('Your balance is not enough');
		             }
	
		            event(new RequestAvailableStoped(['data'=>$data['id'],'token' => []]));
                    $proposal = UParkingProposal::where('buyer_id',$buyer_id)->whereIn('status',['pending','accepted'])->first();

                    if($proposal)throw new NotFoundException('You have already proposed.');
                    $new_proposal = $this->create($request,$available_request->toArray(),$buyer_id);
                    $available_request->delete();

		            $res = $new_proposal->toArray();

                    $user = UUser::where('id',$res['buyer_id'])->with('car')->first();

		            // Creating transaction for buyer
		            UTransaction::create([
            			'phone' => $user->phone,
            			'user_id' => $user->id,
            			'amount' => $price->price,
            			'currency' => 'AMD',
            			'transaction_id' => 'shouldbe',
            			'dt_time' => 'shouldbe',
            			'type' => 'cash',
        	        ]);

		            // Creating transaction for sale
		            $seller = UUser::where('id',$res['seller_id'])->first();
		            $sell_price = UPrice::where('key','sell_price')->first();

		            UTransaction::create([
            			'phone' => $seller->phone,
            			'user_id' => $seller->id,
            			'amount' => $sell_price->price,
            			'currency' => 'AMD',
            			'transaction_id' => 'shouldbe',
            			'dt_time' => 'shouldbe',
            			'type' => 'payment',
        	        ]);


                    $res['color'] = $user->color;
                    $res['car_number'] = $user->car_number;
                    $res['car_model'] = $user->car->make->title .' '. $user->car->title;
		    
	    	
	                $tokens = [];
	                $api_key = 	ApiKey::where('apikeyable_id',$new_proposal->seller_id)->first();
	                if($api_key){
		                $key = $api_key->key;
		                array_push($tokens, $key);
	                }else{
		                array_push($tokens, 'not');
	                }
                    event(new ProposalProposed(['data'=> $res,'token' => $tokens]));

                    return $new_proposal;
                } catch (\Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function feedbackList($request)
    {
        try{
            $feedbacks = UFeedback::get();
            return $feedbacks;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function rate($request)
    {
        try{
            $contract = UContract::where('id', $request['contract_id'])->with('proposal')->first();
            $contract->rating = $request['rate'];
            $contract->save();
            $seller_id = $contract->proposal->seller_id;
            $this->recalculateRating($seller_id);

            return $contract;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function feedback($request)
    {
        try{
            $contract = UContract::where('id', $request['contract_id'])->with('proposal')->first();

            $contract->feedback_id = $request['feedback_id'];
            $contract->save();

            return $contract;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function comment($request)
    {
        try{
            $comment = UComment::create([
               'comment' => $request['comment'],
                'contract_id' => $request['contract_id']
            ]);

            return $comment;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function addToFavourite($request,$user_id)
    {
        try{
            $favourite = UFavourite::create([
                'lat' => $request['lat'],
                'lng' => $request['lng'],
                'user_id' => $user_id,
		        'country' => $request['country'],
                'street' => $request['street']
            ]);

            return $favourite;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function deleteFavourite($favourite_id,$user_id)
    {
        try{
	    $ids = explode( ',', $favourite_id );

	    foreach($ids as $id){
		$favourite = UFavourite::find($id);

            	if(!$favourite)throw new NotFoundException('Favourite not found.');

            	if($favourite->user_id != $user_id)throw new NotFoundException('You have no permission to do that.');

            	$favourite->delete();
	    }
            
        return true;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function recalculateRating($seller_id){

        $contracts = UContract::whereHas('proposal',function($q1) use ($seller_id){
            $q1->whereHas('seller', function($q2) use ($seller_id){
                $q2->where('id' , $seller_id);
            });
        })->get();

        $rate_count = 0;
        $rate_sum = 0;

        foreach($contracts as $contract){
            if(!empty($contract->rating)){
                $rate_sum += $contract->rating;
                $rate_count++;
            }
        }

        $user = UUser::find($seller_id);
        $user->rating = $rate_sum/$rate_count;
        $user->save();
        return true;
    }

    public function finishContract($id,$street,$isCancel,$user_id)
    {
        try{
            $contract = UContract::find($id);
            if(!$contract)throw new NotFoundException('Contract not found.');
	    
	        $proposal = UParkingProposal::find($contract->proposal_id);
            $proposal->status = 'declined';
	        $proposal->save();
	   			
            $contract->status = 'finished';
	        $contract->street = $street;
	        $contract->is_cancelled = $isCancel;
            $contract->finish = date("Y-m-d H:i:s");
            $contract->save();

	        $tokens = [];
	        $api_key = 	ApiKey::where('apikeyable_id',$proposal->seller_id)->first();
	        if($api_key){
		        $key = $api_key->key;
		        array_push($tokens, $key);
	        }else{
		        array_push($tokens, 'not');
	        }

            event(new ProposalFinished(['data'=> $contract,'token' => $tokens]));

            return true;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function history($user_id)
    {
        try{
            $data  = UContract::where('buyer_id',$user_id)->orderBy('created_at', 'desc')->with('prop')->paginate(20);
	    foreach($data as $key=>$value){
	    	$user = UUser::find($value['seller_id']);
	    	$data[$key]->car_number = $user->car_number;
	    }
	    return $data;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function historySale($user_id)
    {
        try{
            $data  = UContract::where('seller_id',$user_id)->orderBy('created_at', 'desc')->with('prop')->paginate(20);

	    foreach($data as $key=>$value){
	    	$user = UUser::find($value['seller_id']);
	    	$data[$key]->car_number = $user->car_number;
	    }

            return $data;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
	

    public function favourites($user_id)
    {
        try{
            $favourites = UFavourite::where('user_id', $user_id)->get();

            return $favourites;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }	

}