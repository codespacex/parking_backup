<?php
namespace Api\V1\General\Services;

use Api\V1\General\Models\UCarMake;
use Api\V1\General\Models\UPrice;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CarMakeService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function getAllCarMakes($request)
    {
        /**
         * Fetch lists by user id
         */
        try {
            $car_makes = UCarMake::orderBy('sortorder', 'ASC')
                ->orderBy('title', 'ASC')
                ->with('models')
                ->get();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $car_makes;
    }

    public function getPrices($request)
    {
        /**
         * Fetch lists by user id
         */
        try {
            $prices = UPrice::get();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $prices;
    }

}