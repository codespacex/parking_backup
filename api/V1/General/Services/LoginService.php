<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Models\UCodeMessage;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Models\UUser;
use Api\V1\General\Models\UContract;


use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class LoginService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function login($request){
        try{
            UCodeMessage::where('phone', $request['phone'])
                ->delete();
            $code = UCodeMessage::generateCode($request['phone']);

            $text = 'The code is ' . $code;
            $this->sendSMS($request['phone'], $text);

            return $code;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function confirm($request){
        try{
            $code = UCodeMessage::where('phone',$request['phone'])->where('code',$request['code'])->first();
            if(!$code)throw new NotFoundException('The code is not found');

            $code->delete();

            $user = UUser::where('phone',$request['phone'])->with('proposals','car')->first();
            $contracts = UContract::where('seller_id',$user->id)->whereNotNull('rating')->get();
	    
	        $count=count($contracts);
	        $user->toArray();
	
	        $user['model'] = $user['car']['title'];
	        $user['make'] = $user['car']['make']['title'];
	        $user['rating_count'] = $count;
	        $car_size = $user['car']['size'];
	        unset($user['car']);

            if(!$user)throw new NotFoundException('User not found');

            //Delete all previous api-keys
            $api_keys = ApiKey::where('apikeyable_id',$user->id)->get();

            foreach ($api_keys as $key){
                $key->delete();
            }

            $apikey = ApiKey::make($user,'user signup',$car_size);

            $data = [];
            $data['user'] = $user;
            $data['key'] = $apikey->key;

            return $data;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function logout($request){
        try{
            $apikey = ApiKey::find($request->apiKey->id);
            if(!$apikey) throw new NotFoundException();
            $apikey->delete();
            return true;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }



    public function sendSMS($phone,$message){

        $str = ltrim($phone, '0');

        $postUrl = "http://31.47.195.66:80/broker/";

        $ch = curl_init();


        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <bulk-request login="topark" password="topark" ref-id="2015-01-22 17:12:55" delivery-notification-requested="true" version="1.0">
                <message id="1" msisdn="+374'.$str.'" service-number="Code" defer-date="2015-01-22 17:12:55" validity-period="3" priority="1">
                <content type="text/plain">'. $message .'</content>
                </message>
                </bulk-request>';

        $header = array("Content-Type:text/xml", "Content-Length:".strlen($xml));

        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$xml);

        // response of the POST request
        $response = curl_exec($ch);

        $responseBody = json_decode($response);

        curl_close($ch);

        return $responseBody;

    }
}