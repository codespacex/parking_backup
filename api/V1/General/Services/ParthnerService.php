<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage UserService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\UTransaction;
use Api\V1\General\Models\UUser;
use Api\Common\Exceptions\Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ParthnerService extends Service
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Inputs' => 'required',
            'Amount' => 'required|numeric',
            'TransactId' => 'required|numeric',
            'Currency' => 'required',
            'DtTime' => 'required'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator1(array $data)
    {
        return Validator::make($data, [
            'Inputs' => 'required',
            'Checksum' => 'required',
            'Lang' => 'required',
            'Currency' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$user)
    {
        return UTransaction::create([
            'phone' => $data['Inputs'][0],
            'user_id' => $user->id,
            'amount' => $data['Amount'],
            'currency' => 'AMD',
            'transaction_id' => $data['TransactId'],
            'dt_time' => $data['DtTime'],
            'type' => 'payment',
        ]);
    }

    public function check($request){
        try{
            if( count($this->validator1($request)->messages()) ){
                throw new NotFoundException($this->validator1($request)->messages());
            }else{
                $phone = $request['Inputs'][0];
                $user = UUser::where('phone',$phone)->first();
                if(!$user) throw new NotFoundException('User not found');

                UUser::recalculateBalance($user->id);
                $user = UUser::where('phone',$phone)->first();

                $data = [];
                $data['ResponseCode'] = 0;
                $data['ResponseMessage'] = 'Գործողությունը թույլատրված է';
                $data['Debt'] = $user->balance;
                $data['PropertyList'][0]['key'] = 'Բաժանորդ';
                $data['PropertyList'][0]['value'] = $user->name;
                $data['PropertyList'][1]['key'] = 'Բաժանորդային համար';
                $data['PropertyList'][1]['value'] = $user->phone;
                $data['Checksum'] = md5('token'.'PropertyList');

                return $data;

            }

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

    }
    public function payment($request){
        try{
            if( count($this->validator($request)->messages()) ){
                throw new NotFoundException($this->validator($request)->messages());
            }else{
                $phone = $request['Inputs'][0];
                $user = UUser::where('phone',$phone)->first();
                if(!$user) throw new NotFoundException('User not found');

                $transaction = $this->create($request,$user);
                UUser::recalculateBalance($user->id);

                $data = [];
                $data['ResponseCode'] = 0;
                $data['ResponseMessage'] = 'Վճարումն ընդունված է';
                $data['PropertyList'][0]['key'] = 'Վճարման կոդ';
                $data['PropertyList'][0]['value'] = $transaction->id;
                $data['Checksum'] = md5('token'.$transaction->dt_time);
                $data['DtTime'] = $transaction->dt_time;

                return $data;
            }

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function decryptIt( $q ) {
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }
}