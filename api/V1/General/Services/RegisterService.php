<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\V1\General\Models\UCodeMessage;
use Api\V1\General\Models\UUser;
use Illuminate\Support\Facades\Validator;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Exceptions\Exception;
use Api\Common\Auth\Models\ApiKey;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class RegisterService extends Service
{

    use RegistersUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone' => 'required|unique:users|min:5',
            'name' => 'required|string|max:255'
        ]);
    }
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorPhone(array $data)
    {
        return Validator::make($data, [
            'phone' => 'required|unique:users|min:5',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorConfirmPhone(array $data)
    {
        return Validator::make($data, [
            'phone' => 'required|unique:users|min:5',
            'code' => 'required|string|max:255'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorEditUser(array $data)
    {
        return Validator::make($data, [
            'car_number' => 'required',
            'car_id' => 'required',
            'name' => 'required'
        ]);
    }	

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'phone' => $data['phone'],
            'car_number' => $data['car_number'],
            'car_id' => $data['car_id'],
            'name' => $data['name'],
            'is_confirmed' => 0
        ]);
    }

    /**
     * Registers new user.
     *
     * @param  array  $data
     * @return \App\User
     */
     public function register($request){

        $checkNotConfirmed = UUser::where('phone',$request['phone'])->where('is_confirmed',0)->first();

        if($checkNotConfirmed){
            $checkNotConfirmed->delete();
        }

        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{

            try{
                $request['car_number'] = '';
                $request['car_id'] = '';
                $user = $this->create($request);
                $code = UCodeMessage::generateCode($request['phone']);
                $text = 'The code is ' . $code;
                $this->sendSMS($request['phone'], $text);
                $data = [];

		        $new_user = UUser::where('id',$user->id)->with('car')->first()->toArray();
		        $new_user['model'] = $new_user['car']['title'];
	    	    $new_user['make'] = $new_user['car']['make']['title'];
	    	    unset($new_user['car']);
		
                $data['user'] = $new_user;
                $data['code'] = $code;

                return $data;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function confirm($request){
        try{
            $code = UCodeMessage::where('phone',$request['phone'])->where('code',$request['code'])->first();
            if(!$code)throw new NotFoundException('The code is not found');
            $code->delete();
            $user = UUser::where('phone',$request['phone'])->first();
	        $user->is_confirmed = 1;

            if(!$user)throw new NotFoundException('User not found');

            return true;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function finish($request){
        try{
            $user = UUser::where('phone',$request['phone'])
                ->where('is_confirmed',0)
                ->first();
            if(!$user)throw new NotFoundException('User not found');

            $user->car_id = $request['car_id'];
            $user->car_number = $request['car_number'];
            $user->color = $request['color'];		
            $user->is_confirmed = 1;
            $user->save();
	    
	    $user2 = UUser::where('id',$user->id)
			    ->with('car')		
                            ->first();		
	    $car_size = !empty($user2->car->size) ? $user2->car->size : 5;

	    $apikey = ApiKey::make($user,'user signup',5);
	    if(!empty($apikey)){
		$apikey->car_size = $car_size;
		$apikey->save();
	    }
	    
	    $data = [];
            $data['user'] = $user;
            $data['key'] = $apikey->key;

	    return $data;		
				
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public function editPhone($request,$user_id){
        if( count($this->validatorPhone($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validatorPhone($request)->messages());
        }else{
            try{
                $user = UUser::where('phone',$request['phone'])
                    ->first();
                if($user)throw new NotFoundException('Another user has used this phone');

                $code = UCodeMessage::generateCode($request['phone']);

                $text = 'The code is ' . $code;
                $this->sendSMS($request['phone'], $text);

                return $user;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function editUser($request,$user_id){
        if( count($this->validatorEditUser($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validatorEditUser($request)->messages());
        }else{
            try{
                $user = UUser::find($user_id);
                if(!$user)throw new NotFoundException('User not found');

                if(!empty($request['phone'])){
                    unset($request['phone']);
                }
                $user->update($request);
		
		$user2 = UUser::where('id',$user->id)
			    ->with('car')		
                            ->first();		
	    $car_size = !empty($user2->car->size) ? $user2->car->size : 5;

	    $apikey = ApiKey::where('apikeyable_id',$user2->id)->first();
	    if(!empty($apikey)){
		$apikey->car_size = $car_size;
		$apikey->save();
	    }


                return $user2;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function confirmEditPhone($request,$user_id){
        if( count($this->validatorConfirmPhone($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validatorConfirmPhone($request)->messages());
        }else{
            try{
                $code = UCodeMessage::where('phone',$request['phone'])
                    ->where('code',$request['code'])
                    ->first();
                if(!$code)throw new NotFoundException('The code is not found');

                $user = UUser::find($user_id);
                $user->phone = $request['phone'];
                $user->save();

                return $user;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }		

    public function sendSMS($phone,$message){

        $str = ltrim($phone, '0');

        $postUrl = "http://31.47.195.66:80/broker/";

        $ch = curl_init();


        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <bulk-request login="topark" password="topark" ref-id="2015-01-22 17:12:55" delivery-notification-requested="true" version="1.0">
                <message id="1" msisdn="+374'.$str.'" service-number="Code" defer-date="2015-01-22 17:12:55" validity-period="3" priority="1">
                <content type="text/plain">'. $message .'</content>
                </message>
                </bulk-request>';

        $header = array("Content-Type:text/xml", "Content-Length:".strlen($xml));

        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$xml);

        // response of the POST request
        $response = curl_exec($ch);

        $responseBody = json_decode($response);

        curl_close($ch);

        return $responseBody;

    }
}