<?php
namespace Api\V1\General\Services;

use Api\Common\Models\AvailableRequests;
use Api\Common\Models\Contract;
use Api\V1\General\Models\UAvailableRequests;
use Api\V1\General\Models\UCarMake;
use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UParkingProposal;
use App\Events\LocationSent;
use App\Events\ProposalAccepted;
use App\Events\ProposalDeclined;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidUserRegistrationException;
use Api\Common\Exceptions\NotFoundException;
use Carbon\Carbon;
use App\Events\RequestAvailableStarted;
use App\Events\RequestAvailableStoped;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Models\UUser;
use Api\Common\Auth\Models\ApiKey;

/**
 * Class SalonService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage SalonService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class SellService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lng' => 'required|regex:/^-?\d{1,2}\.\d{6,}$/',
            'lat' => 'required|regex:/^-?\d{1,2}\.\d{6,}$/'
        ]);
    }

/**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$user_id)
    {
        return UAvailableRequests::create([
            'lng' => $data['lng'],
            'lat' => $data['lat'],
            'user_id' => $user_id,
            'rating' => $data['rating'],
            'rating_count' => $data['rating_count'],
            'car_number' => $data['car_number'],
            'color' => $data['color'],
            'phone' => $data['phone'],
            'car_size' => $data['car_size'],
        ]);
    }

    public function becomeAvailable($request,$user_id)
    {
        if( count($this->validator($request)->messages()) ){
            throw new InvalidUserRegistrationException($this->validator($request)->messages());
        }else{
            try{
                $user = UUser::where('id',$user_id)->with('car','sold')->first();
                if(!$user){
                    throw new NotFoundException('User not found.');
                }else{
		            $contracts = UContract::where('seller_id',$user->id)->where('status','finished')->whereNotNull('rating')->get();
                    $request['rating'] = $user->rating;
                    $request['rating_count'] = count($contracts);
		            $request['car_number'] = $user->car_number;
                    $request['color'] = $user->color;
                    $request['phone'] = $user->phone;
                    $request['car_size'] = $user->car->size;
                }
                $available_exists_ids = UAvailableRequests::where('user_id',$user_id)->pluck('id')->toArray();
                !empty($available_exists_ids) ? UAvailableRequests::destroy($available_exists_ids) : false;
                $avail = $this->create($request,$user_id);
		        $new = $avail->toArray();
		        $tokens = ApiKey::where('car_size','<=',$user->car->size)->where('apikeyable_id','!=',$user_id)->pluck('key')->toArray();
                event(new RequestAvailableStarted(['data' => $new, 'token' => $tokens]));

                return true;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function stopAvailable($request,$user_id)
    {
        try{
            $available_exists_ids = UAvailableRequests::where('user_id',$user_id)->pluck('id')->toArray();

	        if(!empty($available_exists_ids[0])){
		        event(new RequestAvailableStoped(['data'=>$available_exists_ids[0],'token' => []]));
	        }
            !empty($available_exists_ids) ? UAvailableRequests::destroy($available_exists_ids) : false;
            return true;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function acceptProposal($proposal_id,$user_id)
    {
        try{
            $proposal = UParkingProposal::where('id',$proposal_id)->where('status','pending')->with('request')->first();
            if(!$proposal)throw new NotFoundException('Proposal not found.');

            if($proposal->seller_id!= $user_id) throw new NotFoundException('You cannot accept this proposal.');

            $proposal->status = 'accepted';
            $proposal->save();
            $proposal_socket = $proposal->toArray();
            $now = date("Y-m-d H:i:s");
            $contract = UContract::create([
                'proposal_id' => $proposal_id,
                'status' => 'in_process',
                'start' => $now,
		        'buyer_id' => $proposal->buyer_id,
                'seller_id' => $proposal->seller_id
            ]);
            $contract->save();

	        $user = UUser::where('id',$proposal->seller_id)->with('car')->first();
            $proposal_socket['contract_id'] = $contract->id;
            $proposal_socket['color'] = $user->color;
            $proposal_socket['phone'] = $user->phone;
            $proposal_socket['car_number'] = $user->car_number;
            $proposal_socket['model'] = $user->car->title;
            $proposal_socket['make'] = $user->car->make->title;
	    
            $tokens = [];
            $api_key = 	ApiKey::where('apikeyable_id',$proposal->buyer_id)->first();
            if($api_key){
                $key = $api_key->key;
                array_push($tokens, $key);
            }else{
                array_push($tokens, 'not');
            }
            event(new ProposalAccepted(['data'=>$proposal_socket,'token' => $tokens]));
            return true;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function declineProposal($proposal_id,$user_id)
    {
        try{
            $proposal = UParkingProposal::where('id',$proposal_id)->where('status','pending')->with('request')->first();
            if(!$proposal)throw new NotFoundException('Proposal not found.');
            if($proposal->seller_id != $user_id) throw new NotFoundException('You cannot accept this proposal.');
            $proposal->status = 'declined';
            $proposal->save();
            $user = UUser::where('id', $proposal->seller_id)->with('car','sold')->first();

            $availableDec = AvailableRequests::create([
                'user_id' => $proposal->seller_id,
                'lng' => $proposal->lng,
                'lat' => $proposal->lat,
                'rating_count' => count($user->sold),
                'car_number' => $user->car_number,
                'color' => $user->color,
                'phone' => $user->phone,
                'rating' => $user->rating,
                'car_size' => $user->car->size
            ]);
            $new = $availableDec->toArray();


            $tokens_available = ApiKey::where('car_size','<=',$user->car->size)->where('apikeyable_id','!=',$user_id)->pluck('key')->toArray();

            event(new RequestAvailableStarted(['data'=>$new,'token' => $tokens_available]));

            $tokens = [];
            $api_key = 	ApiKey::where('apikeyable_id',$proposal->buyer_id)->first();
            if($api_key){
                $key = $api_key->key;
                array_push($tokens, $key);
            }else{
                array_push($tokens, 'not');
            }

            event(new ProposalDeclined(['data'=>$proposal,'token' => $tokens]));

            return true;
            } catch (\Exception $e) {
                throw new Exception($e->getMessage());
            }
    }

    public function sendLocation($request,$user_id){

	    $data['contract_id'] = $request['contract_id'];
        $data['lat'] = $request['lat'];
        $data['lng'] = $request['lng'];
        $data['from'] = $user_id;
        $data['to'] = $request['user_id'];
	
	    $tokens = [];
	    $api_key = 	ApiKey::where('apikeyable_id',$request['user_id'])->first();
	    if($api_key){
		    $key = $api_key->key;
		    array_push($tokens, $key);
	    }else{
		    array_push($tokens, 'not');
	    }

        event(new LocationSent(['data'=>$data,'token' => $tokens]));
	    return true;
    }

}