<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage UserService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\UUser;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UParkingProposal;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserService extends Service
{
    public function update($request, $user_id){
        try{
            $user = UUser::find($user_id);

            if(!$user) throw new NotFoundException('User not found.');

            if($user->id != $user_id) throw new NotFoundException('User not found.');

            if(array_key_exists ( 'is_confirmed' ,  $request )) unset($request['is_confirmed']);

            return $user->update($request);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

    }
    
    public function getUser($id){
        //try{
	    UUser::recalculateBalance($id);
        $user = UUser::where('id',$id)->with('car')->first();
	    if(!$user) throw new NotFoundException('User not found.');
        $contracts = UContract::where('seller_id',$user->id)->where('status','finished')->whereNotNull('rating')->get();
	    $count=count($contracts);
	    $user->toArray();
	
	    $user['model'] = $user['car']['title'];
	    $user['make'] = $user['car']['make']['title'];	
	    $user['rating_count'] = $count;
	    unset($user['car']);
	    $user['status'] = 'free';
	    
	    $checkPending = UParkingProposal::where('buyer_id',$id)->where('status','pending')->first();
	    if($checkPending ){

            $res = $checkPending->toArray();
            $user['status'] = 'buy_proposal';

            $user1 = UUser::where('id',$res['seller_id'])->with('car')->first();

            $res['color'] = $user1->color;
            $res['car_number'] = $user1->car_number;
            $res['car_model'] = $user1->car->make->title .' '. $user1->car->title;
            $user['buy_proposal'] = $res;
	    }

	    $checkPendingSell = UParkingProposal::where('seller_id',$id)->where('status','pending')->first();
	    	
	    if($checkPendingSell){
            $res = $checkPendingSell->toArray();
            $user['status'] = 'sell_proposal';

            $user1 = UUser::where('id',$res['buyer_id'])
                                            ->with('car')
                        ->first();

                    $res['color'] = $user1->color;
                    $res['car_number'] = $user1->car_number;
                    $res['car_model'] = $user1->car->make->title .' '. $user1->car->title;
            $user['sell_proposal'] = $res;
	    }
	    
	    $checkContractSell = UContract::where('seller_id',$id)->with('proposal')->where('status','in_process')->first();
	    if($checkContractSell){
            $res = $checkContractSell->toArray();
            $user['status'] = 'sell_contract';

            $user1 = UUser::where('id',$res['buyer_id'])
                                            ->with('car')
                        ->first();
            $res['minutes'] = $res['proposal']['minutes'];
            unset($res['proposal']);
                    $res['color'] = $user1->color;
                    $res['car_number'] = $user1->car_number;
                    $res['car_model'] = $user1->car->make->title .' '. $user1->car->title;
            $user['sell_contract'] = $res;
	    }
	    
	    $checkContractBuy = UContract::where('buyer_id',$id)->with('proposal')->where('status','in_process')->first();
	    if($checkContractBuy){
            $res = $checkContractBuy->toArray();
            $user['status'] = 'buy_contract';

            $user1 = UUser::where('id',$res['buyer_id'])
                                            ->with('car')
                        ->first();
            $res['minutes'] = $res['proposal']['minutes'];
            unset($res['proposal']);
                    $res['color'] = $user1->color;
                    $res['car_number'] = $user1->car_number;
                    $res['car_model'] = $user1->car->make->title .' '. $user1->car->title;
            $user['buy_contract'] = $res;
	    }

            return $user;
        

    }

}