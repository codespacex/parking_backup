<?php

namespace App\Console\Commands;

use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UParkingProposal;
use Illuminate\Console\Command;
use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\UUser;
use Api\Common\Models\AvailableRequests;
use Api\Common\Auth\Models\ApiKey;
use App\Events\RequestAvailableStarted;
use App\Events\ProposalDeclined;

class CheckContract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-contract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Contracts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contracts = UContract::whereIn('status',['in_process','accepted'])->with('proposal')->get();

        foreach ($contracts as $contract){

            $minutes_to_add = $contract->proposal->minutes;
            $finish_time = strtotime('+'. $minutes_to_add .' minutes', strtotime($contract->start));
            $now = strtotime(date('Y-m-d H:i:s'));

            if($finish_time < $now){
                $contract = UContract::find($contract->id);
                if(!$contract)throw new NotFoundException('Contract not found.');

                $proposal = UParkingProposal::find($contract->proposal_id);

                $proposal->status = 'declined';
                $proposal->save();


                $contract->status = 'finished';
                $contract->street = 'No address';
                $contract->finish = date("Y-m-d H:i:s");
                $contract->save();

                $tokens = [];
                $api_key = 	ApiKey::where('apikeyable_id',$proposal->seller_id)->first();
                if($api_key){
                    $key = $api_key->key;
                    array_push($tokens, $key);
                }else{
                    array_push($tokens, 'not');
                }

                event(new ProposalFinished(['data'=> $contract,'token' => $tokens]));

            }
        }
    }
}
