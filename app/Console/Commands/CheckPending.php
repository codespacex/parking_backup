<?php

namespace App\Console\Commands;

use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UParkingProposal;
use Illuminate\Console\Command;
use Api\Common\Exceptions\NotFoundException;
use Api\V1\General\Models\UUser;
use Api\Common\Models\AvailableRequests;
use Api\Common\Auth\Models\ApiKey;
use App\Events\RequestAvailableStarted;
use App\Events\ProposalDeclined;

class CheckPending extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Pending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $proposals = UParkingProposal::where('status','pending')->get();

        foreach ($proposals as $proposal){

            $minutes_to_add = $proposal->minutes;
            $finish_time = strtotime('+'. $minutes_to_add .' minutes', strtotime($proposal->created_at));
            $now = strtotime(date('Y-m-d H:i:s'));

            if($finish_time < $now){
                $proposal = UParkingProposal::where('id',$proposal->id)
                    ->where('status','pending')
                    ->with('request')
                    ->first();
                if(!$proposal)throw new NotFoundException('Proposal not found.');

                $proposal->status = 'declined';
                $proposal->save();

                $user = UUser::where('id', $proposal->seller_id)->with('car','sold')->first();

                $availableDec = AvailableRequests::create([
                    'user_id' => $proposal->seller_id,
                    'lng' => $proposal->lng,
                    'lat' => $proposal->lat,
                    'rating_count' => count($user->sold),
                    'car_number' => $user->car_number,
                    'color' => $user->color,
                    'phone' => $user->phone,
                    'rating' => $user->rating,
                    'car_size' => $user->car->size
                ]);
                $new = $availableDec->toArray();


                $tokens_available = ApiKey::where('car_size','<=',$user->car->size)->where('apikeyable_id','!=',$user->id)->pluck('key')->toArray();

                event(new RequestAvailableStarted(['data'=>$new,'token' => $tokens_available]));

                $tokens = [];
                $api_key = 	ApiKey::where('apikeyable_id',$proposal->buyer_id)->first();
                if($api_key){
                    $key = $api_key->key;
                    array_push($tokens, $key);
                }else{
                    array_push($tokens, 'not');
                }

                event(new ProposalDeclined(['data'=>$proposal,'token' => $tokens]));

            }
        }
    }
}
