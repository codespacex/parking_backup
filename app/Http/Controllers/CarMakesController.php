<?php

namespace App\Http\Controllers;

use Api\Common\Models\CarMake;
use App\Models\ACarMake;
use Illuminate\Http\Request;

class CarMakesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_makes = ACarMake::orderBy('sortorder', 'ASC')
            ->orderBy('title', 'ASC')
            ->get()
            ->toArray();

        return view('car_makes.car_makes',compact('car_makes'));
    }

    public function add()
    {
        return view('car_makes.add',compact('car_make'));
    }

    public function edit($id)
    {
        $car_make = ACarMake::find($id);

        return view('car_makes.add',compact('car_make'));
    }

    public function create(Request $request){
        if(!empty($request->title)){
            ACarMake::create([
                'title' => $request->title,
                'sortorder' => $request->sortorder
            ]);
        }
        return redirect()->route('car-makes');
    }

    public function update(Request $request){

        $car_make = ACarMake::find($request->id);

        if(!empty($request->title)){
            $car_make->title = $request->title;
            $car_make->sortorder = $request->sortorder;
            $car_make->save();
        }
        return redirect()->route('car-makes');
    }

    public function delete($id)
    {
        $car_make = ACarMake::find($id);
        $car_make->delete();

        return redirect()->route('car-makes');
    }
}
