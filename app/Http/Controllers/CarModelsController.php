<?php

namespace App\Http\Controllers;

use Api\Common\Models\CarMake;
use App\Models\ACarMake;
use App\Models\ACarModel;
use Illuminate\Http\Request;

class CarModelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_models = ACarModel::with('make')
            ->get()
            ->toArray();

        return view('car_models.car_models',compact('car_models'));
    }

    public function add()
    {
        $car_makes = ACarMake::get()->toArray();

        return view('car_models.add',compact('car_makes'));
    }

    public function edit($id)
    {
        $car_makes = ACarMake::get()->toArray();
        $car_model = ACarModel::find($id);

        return view('car_models.add',compact('car_makes','car_model'));
    }

    public function create(Request $request){
        if(!empty($request->title)){
            ACarModel::create([
                'title' => $request->title,
                'size' => $request->size,
                'car_make_id' => $request->car_make_id
            ]);
        }
        return redirect()->route('car-models');
    }

    public function update(Request $request){

        $car_model = ACarModel::find($request->id);
        if(!empty($request->title)){
            $car_model->title = $request->title;
            $car_model->size = $request->size;
            $car_model->car_make_id = $request->car_make_id;
            $car_model->save();
        }
        return redirect()->route('car-models');
    }

    public function delete($id)
    {
        $car_make = ACarModel::find($id);
        $car_make->delete();

        return redirect()->route('car-models');
    }
}
