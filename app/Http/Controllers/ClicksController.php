<?php

namespace App\Http\Controllers;

use Api\Common\Models\ButtonClick;
use Api\V1\General\Models\UContract;

class ClicksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sort)
    {
        if(isset($sort)){
            switch ($sort) {
                case 'all':
                    $now = date('Y-m-d H:i:s');
                    $year_before = date("Y-m-d H:i:s", strtotime("-10 year", time()));

                    $find = $this->getFind($year_before,$now);
                    $sell = $this->getSell($year_before,$now);

                    break;
                case 'year':

                    $now = date('Y-m-d H:i:s');
                    $year_before = date("Y-m-d H:i:s", strtotime("-1 year", time()));

                    $find = $this->getFind($year_before,$now);
                    $sell = $this->getSell($year_before,$now);

                    break;
                case 'month':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-30 days", time()));

                    $find = $this->getFind($month_before,$now);
                    $sell = $this->getSell($month_before,$now);
                    break;
                case 'week':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-7 days", time()));

                    $find = $this->getFind($month_before,$now);
                    $sell = $this->getSell($month_before,$now);

                    break;
                case 'today':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-1 day", time()));

                    $find = $this->getFind($month_before,$now);
                    $sell = $this->getSell($month_before,$now);

                    break;
            }
        }else{
            return redirect()->route('clicks');
        }

        return view('clicks.clicks',compact('find','sell','sort'));
    }

    public function getFind($from,$to){
        return ButtonClick::whereBetween('created_at', [$from, $to])
            ->where('button_type','find')
            ->get();
    }

    public function getSell($from,$to){
        return ButtonClick::whereBetween('created_at', [$from, $to])
            ->where('button_type','sell')
            ->get();
    }
}
