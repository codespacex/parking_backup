<?php

namespace App\Http\Controllers;

use Api\V1\General\Models\UFeedback;
use App\Models\AContract;
use Illuminate\Http\Request;
use Api\V1\General\Models\UContract;
use Api\V1\General\Models\UUser;

class ContractsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contracts = AContract::with('proposal')
            ->paginate(10);

        return view('contracts.contracts',compact('contracts'));
    }

    public function edit($id)
    {
        $contract = AContract::where('id',$id)
                                ->with('proposal')
                                ->first();

        $feedbacks = UFeedback::all();

        $seller_id = $contract->proposal->seller->id;
        $buyer_id = $contract->proposal->buyer->id;

        $seller = [];
        $seller['bought'] = UContract::whereHas('proposal',function($q1) use ($seller_id){
            $q1->whereHas('buyer', function($q2) use ($seller_id){
                $q2->where('id' , $seller_id);
            });
        })->get();
        $seller['sold'] = UContract::whereHas('proposal',function($q1) use ($seller_id){
            $q1->whereHas('seller', function($q2) use ($seller_id){
                $q2->where('id' , $seller_id);
            });
        })->get();

        $buyer = [];
        $buyer['bought'] = UContract::whereHas('proposal',function($q1) use ($buyer_id){
            $q1->whereHas('buyer', function($q2) use ($buyer_id){
                $q2->where('id' , $buyer_id);
            });
        })->get();
        $buyer['sold'] = UContract::whereHas('proposal',function($q1) use ($buyer_id){
            $q1->whereHas('seller', function($q2) use ($buyer_id){
                $q2->where('id' , $buyer_id);
            });
        })->get();

        return view('contracts.add',compact('contract','feedbacks','buyer','seller'));
    }


    public function update(Request $request){

        $contract = UContract::where('id',$request->id)
                                ->with('proposal')
                                ->first();

        $seller_id = $contract->proposal->seller->id;

        $contract->status = $request->status;
        $contract->rating = $request->rating;
        $contract->feedback_id = $request->feedback_id;
        $contract->save();

        $this->recalculateRating($seller_id);

        return redirect()->route('contracts');
    }

    public function recalculateRating($seller_id){

        $contracts = UContract::whereHas('proposal',function($q1) use ($seller_id){
            $q1->whereHas('seller', function($q2) use ($seller_id){
                $q2->where('id' , $seller_id);
            });
        })->get();

        $rate_count = 0;
        $rate_sum = 0;

        foreach($contracts as $contract){
            if(!empty($contract->rating)){
                $rate_sum += $contract->rating;
                $rate_count++;
            }
        }
        if($contracts){
            $user = UUser::find($seller_id);
            $user->rating = $rate_sum/$rate_count;
            $user->save();
        }

        return true;
    }

}
