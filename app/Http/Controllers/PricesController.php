<?php

namespace App\Http\Controllers;

use Api\Common\Models\Price;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Price::get()->toArray();

        return view('prices.prices',compact('prices'));
    }

    public function edit($id)
    {
        $price = Price::find($id);

        return view('prices.add',compact('price'));
    }

    public function update(Request $request){

        $price = Price::find($request->id);

        if(!empty($request->price)){
            $price->price = $request->price;
            $price->save();
        }
        return redirect()->route('prices');
    }
}
