<?php

namespace App\Http\Controllers;

use Api\V1\General\Models\UContract;

class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sort)
    {
        if(isset($sort)){
            switch ($sort) {
                case 'all':
                    $now = date('Y-m-d H:i:s');
                    $year_before = date("Y-m-d H:i:s", strtotime("-10 year", time()));

                    $contracts = $this->getAll($year_before,$now);
                    $contracts_five= $this->getFive($year_before,$now);
                    $contracts_other_rate = $this->getOtherRate($year_before,$now);
                    $contracts_not_rated = $this->getNotRated($year_before,$now);
                    $contracts_not_in_the_place = $this->getNotInPlace($year_before,$now);
                    $contracts_driver_not_in_the_place = $this->getDriverNotInPlace($year_before,$now);
                    $contracts_time_not_enough = $this->getTimeNotEnough($year_before,$now);

                    break;
                case 'year':

                    $now = date('Y-m-d H:i:s');
                    $year_before = date("Y-m-d H:i:s", strtotime("-1 year", time()));

                    $contracts = $this->getAll($year_before,$now);
                    $contracts_five= $this->getFive($year_before,$now);
                    $contracts_other_rate = $this->getOtherRate($year_before,$now);
                    $contracts_not_rated = $this->getNotRated($year_before,$now);
                    $contracts_not_in_the_place = $this->getNotInPlace($year_before,$now);
                    $contracts_driver_not_in_the_place = $this->getDriverNotInPlace($year_before,$now);
                    $contracts_time_not_enough = $this->getTimeNotEnough($year_before,$now);

                    break;
                case 'month':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-30 days", time()));

                    $contracts = $this->getAll($month_before,$now);
                    $contracts_five= $this->getFive($month_before,$now);
                    $contracts_other_rate = $this->getOtherRate($month_before,$now);
                    $contracts_not_rated = $this->getNotRated($month_before,$now);
                    $contracts_not_in_the_place = $this->getNotInPlace($month_before,$now);
                    $contracts_driver_not_in_the_place = $this->getDriverNotInPlace($month_before,$now);
                    $contracts_time_not_enough = $this->getTimeNotEnough($month_before,$now);

                    break;
                case 'week':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-7 days", time()));

                    $contracts = $this->getAll($month_before,$now);
                    $contracts_five= $this->getFive($month_before,$now);
                    $contracts_other_rate = $this->getOtherRate($month_before,$now);
                    $contracts_not_rated = $this->getNotRated($month_before,$now);
                    $contracts_not_in_the_place = $this->getNotInPlace($month_before,$now);
                    $contracts_driver_not_in_the_place = $this->getDriverNotInPlace($month_before,$now);
                    $contracts_time_not_enough = $this->getTimeNotEnough($month_before,$now);

                    break;
                case 'today':
                    $now = date('Y-m-d H:i:s');
                    $month_before = date("Y-m-d H:i:s", strtotime("-1 day", time()));

                    $contracts = $this->getAll($month_before,$now);
                    $contracts_five= $this->getFive($month_before,$now);
                    $contracts_other_rate = $this->getOtherRate($month_before,$now);
                    $contracts_not_rated = $this->getNotRated($month_before,$now);
                    $contracts_not_in_the_place = $this->getNotInPlace($month_before,$now);
                    $contracts_driver_not_in_the_place = $this->getDriverNotInPlace($month_before,$now);
                    $contracts_time_not_enough = $this->getTimeNotEnough($month_before,$now);

                    break;
            }
        }else{
            return redirect()->route('contracts');
        }

        return view('statistics.statistics',compact('contracts','contracts_five','contracts_other_rate','contracts_not_rated','contracts_not_in_the_place','contracts_driver_not_in_the_place','contracts_time_not_enough','sort'));
    }

    public function getAll($from,$to){
        return UContract::whereBetween('start', [$from, $to])
        ->get();
    }
    public function getFive($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->where('rating',5)
            ->where('status','finished')
            ->get();
    }
    public function getOtherRate($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->whereIn('rating',[1,2,3,4])
            ->where('status','finished')
            ->get();
    }
    public function getNotRated($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->where('rating',NULL)
            ->where('status','finished')
            ->get();
    }
    public function getNotInPlace($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->where('feedback_id',1)
            ->where('status','finished')
            ->get();
    }
    public function getDriverNotInPlace($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->where('feedback_id',2)
            ->where('status','finished')
            ->get();
    }
    public function getTimeNotEnough($from,$to){
        return UContract::whereBetween('start', [$from, $to])
            ->where('feedback_id',3)
            ->where('status','finished')
            ->get();
    }
}
