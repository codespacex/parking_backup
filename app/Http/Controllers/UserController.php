<?php

namespace App\Http\Controllers;

use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Models\UCarModel;
use Api\V1\General\Models\UParkingProposal;
use Api\V1\General\Models\UUser;
use App\Models\AUser;
use Api\V1\General\Models\UContract;
use App\Models\ACarMake;
use Carbon;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = AUser::with('car')
                 ->get()
                 ->toArray();

        return view('users.users',compact('users'));
    }

    public function edit($id)
    {
        $user = UUser::where('id',$id)
            ->with('car')
            ->first();

        $contracts = [];

        $contracts['bought'] = UContract::whereHas('proposal',function($q1) use ($id){
            $q1->whereHas('buyer', function($q2) use ($id){
                $q2->where('id' , $id);
            });
        })->get();
        $contracts['sold'] = UContract::whereHas('proposal',function($q1) use ($id){
            $q1->whereHas('seller', function($q2) use ($id){
                $q2->where('id' , $id);
            });
        })->get();

        $car_makes = ACarMake::get()->toArray();

        $models = UCarModel::where('car_make_id',$user->car->car_make_id)
                             ->get()
                             ->toArray();

        return view('users.add',compact('user','car_makes','contracts','models'));
    }

    public function update(Request $request)
    {
        $user = UUser::find($request->id);

        $user->name = $request->name;
        $user->car_number = $request->car_number;
        $user->car_id = $request->car_id;
        $user->phone = $request->phone;
        $user->save();

        return redirect()->route('home');
    }

    public function carSizes(Request $request)
    {
        $users = UUser::with('car')->where('is_confirmed',1)->get();
        if($users){
            $cars = [];
            $cars['1']['size'] = 3.8;
            $cars['1']['count'] = 0;

            $cars['2']['size'] = 4.2;
            $cars['2']['count'] = 0;

            $cars['3']['size'] = 4.5;
            $cars['3']['count'] = 0;

            $cars['4']['size'] = 4.8;
            $cars['4']['count'] = 0;

            $cars['5']['size'] = 5.1;
            $cars['5']['count'] = 0;

            foreach($users as $user){
                if(!empty($user->car->size)){
                    $cars[$user->car->size]['count']++;
                }
            }

            foreach ($cars as $key=>$size){
                $cars[$key]['percent'] = $size['count']/count($users)*100;
            }

            return view('users.carsizes',compact('cars','users'));
        }
        return redirect()->route('home');
    }

    public function find(Request $request)
    {
        $users = UUser::where('phone', 'like', '%' . $request->number . '%')
                        ->orWhere('car_number', 'like', '%' . $request->number . '%')
                        ->orWhere('name', 'like', '%' . $request->number . '%')
                        ->with('car')
                        ->get()
                        ->toArray();

        $word = $request->number;
        return view('users.users',compact('users','word'));
    }

    public function block($id)
    {
        $user = UUser::where('id',$id)
            ->first();

        $user->is_blocked = 1;
        $user->save();

        $api_keys = ApiKey::where('apikeyable_id',$user->id)
                        ->get();

        foreach ($api_keys as $key){
            $key->delete();
        }

        return redirect()->route('home');
    }

    public function unblock($id)
    {
        $user = UUser::where('id',$id)
            ->first();

        $user->is_blocked = 0;
        $user->save();

        return redirect()->route('home');
    }
}
