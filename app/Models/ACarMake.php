<?php

namespace App\Models;

use Api\Common\Models\CarMake;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ACarMake extends CarMake
{
    protected $table = 'car_makes';

}
