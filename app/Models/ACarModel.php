<?php

namespace App\Models;

use Api\Common\Models\CarModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ACarModel extends CarModel
{
    protected $table = 'car_models';

    public function make() {
        return $this->hasOne(ACarMake::class , 'id','car_make_id');
    }
}
