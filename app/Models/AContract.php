<?php

namespace App\Models;

use Api\Common\Models\Contract;
use Api\V1\General\Models\UParkingProposal;

class AContract extends Contract
{
    protected $table = 'contracts';

    public function proposal() {
        return $this->hasOne(UParkingProposal::class , 'id','proposal_id')->with('request','buyer','seller');
    }
}
