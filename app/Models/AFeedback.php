<?php

namespace App\Models;

use Api\Common\Models\Contract;
use Api\Common\Models\Feedback;
use Api\V1\General\Models\UParkingProposal;

class AFeedback extends Feedback
{
    protected $table = 'feedbacks';
}
