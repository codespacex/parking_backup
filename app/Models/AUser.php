<?php

namespace App\Models;

use App\User;

class AUser extends User
{
    protected $table = 'users';

    public function car() {
        return $this->hasOne(ACarModel::class , 'id','car_id')->with('make');
    }
}
