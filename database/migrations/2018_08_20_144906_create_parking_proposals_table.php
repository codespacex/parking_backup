<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id');
            $table->integer('request_id');
            $table->integer('minutes');
            $table->enum('status', ['pending', 'accepted', 'declined','finished']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_proposals');
    }
}
