<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerIdToProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_proposals', function (Blueprint $table) {
            $table->integer('seller_id')->nullable()->default(NULL);
            $table->decimal('lng', 10, 7);
            $table->decimal('lat', 10, 7);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_proposals', function (Blueprint $table) {
            $table->dropColumn('seller_id');
            $table->dropColumn('lng');
            $table->dropColumn('lat');
        });
    }
}
