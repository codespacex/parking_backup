<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAvailableRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('available_requests', function (Blueprint $table) {
            $table->decimal('rating',10, 2)->nullable()->default(null);
            $table->integer('rating_count')->nullable()->default(null);
            $table->string('car_number')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->integer('car_size')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('available_requests', function (Blueprint $table) {
            $table->dropColumn('rating');
            $table->dropColumn('rating_count');
            $table->dropColumn('car_number');
            $table->dropColumn('color');
            $table->dropColumn('phone');
            $table->dropColumn('car_size');
        });
    }
}
