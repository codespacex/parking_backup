@extends('layouts.admin')

@section('content')
    <?php
    $action = '/car-makes/create';
    $title = '';
    $sortorder = 99;

    if(!empty($car_make)){
        $action = '/car-makes/update';
        $title = $car_make->title;
        $sortorder = $car_make->sortorder;
    }
    ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header"><strong>Add car make</strong></div>
            <form method="post" action="<?= $action ?>" class="card-body card-block">
                <a href="/car-makes"><div class="btn btn-primary">Back</div></a>
                <?php if(!empty($car_make)){ ?>
                     <input type="hidden" name="id" value="<?= $car_make->id ?>">
                <?php } ?>
                <div class="form-group"><label for="company" class=" form-control-label">Title</label><input type="text" name="title" id="company" placeholder="Enter title" class="form-control" value="<?= $title ?>"></div>
                <div class="form-group">
                    <label for="sortorder" class=" form-control-label">Sortorder</label>
                    <input type="number" name="sortorder" id="sortorder" placeholder="Enter title" class="form-control" value="<?= $sortorder ?>">
                </div>
                {{ csrf_field() }}
                <button class="btn btn-success" type="submit">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection