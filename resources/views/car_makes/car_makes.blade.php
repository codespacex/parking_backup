@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Car makes</strong>
            <div class="com-md-2">
                <a href="/car-makes/add"><div class="btn btn-success">Add car make</div></a>
            </div>
        </div>
        <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Make</th>
                    <th>Sortorder</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($car_makes as $car_make){ ?>
                <tr>
                    <td><?= $car_make['id'] ?></td>
                    <td><?= $car_make['title'] ?></td>
                    <td><?= $car_make['sortorder'] ?></td>
                    <td><a href="/car-makes/edit/<?= $car_make['id'] ?>"><i class="fa fa-edit"></i></a> <a href="/car-makes/delete/<?= $car_make['id'] ?>"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
