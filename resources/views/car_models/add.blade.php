@extends('layouts.admin')

@section('content')
    <?php
    $action = '/car-models/create';
    $title = '';
    $model = '';
    $model_size = '';

    if(!empty($car_model)){
        $action = '/car-models/update';
        $title = $car_model->title;
        $model = $car_model->car_make_id;
        $model_size = $car_model->size;
    }

    $size = ['1'=>'3.8','2'=>'4.2','3'=>'4.5','4'=>'4.8','5'=>'5.1'];
    ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><strong>Add car model</strong></div>
                <form method="post" action="<?= $action ?>" class="card-body card-block">
                    <a href="/car-models"><div class="btn btn-primary">Back</div></a>
                    <?php if(!empty($car_model)){ ?>
                        <input type="hidden" name="id" value="<?= $car_model->id ?>">
                    <?php } ?>
                    <div class="form-group"><label for="company" class=" form-control-label">Title</label><input type="text" name="title" id="company" placeholder="Enter title" class="form-control" value="<?= $title ?>"></div>
                    <div class="form-group"><label for="car_make_id" class=" form-control-label">Car Make</label>
                        <select name="car_make_id" id="car_make_id" class="form-control-lg form-control" value="<?= $model ?>">
                            <?php foreach ($car_makes as $car_make){ ?>
                                <?php if($model == $car_make['id']){ ?>
                                    <option selected value="<?= $car_make['id'] ?>"><?= $car_make['title'] ?></option>
                                <?php }else{ ?>
                                    <option value="<?= $car_make['id'] ?>"><?= $car_make['title'] ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group"><label for="size" class=" form-control-label">Size</label>
                        <select name="size" id="size" class="form-control-lg form-control" value="<?= $model_size ?>">
                            <?php foreach ($size as $key => $value){ ?>
                                <?php if($model_size == $key){ ?>
                                    <option selected value="<?= $key ?>"><?= $value ?></option>
                                <?php }else{ ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection