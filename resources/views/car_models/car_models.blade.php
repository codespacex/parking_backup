@extends('layouts.admin')

@section('content')

<?php
    $size = ['1'=>'3.8','2'=>'4.2','3'=>'4.5','4'=>'4.8','5'=>'5.1'];
?>
<div class="card">
    <div class="card-header">
        <strong class="card-title">Car models</strong>
        <div class="com-md-2">
            <a href="/car-models/add"><div class="btn btn-success">Add car model</div></a>
        </div>
    </div>
    <div class="card-body">
        <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Id</th>
                <th>Make</th>
                <th>Title</th>
                <th>Size</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($car_models as $car_model){ ?>
                <tr>
                    <td><?= $car_model['id'] ?></td>
                    <td><?= $car_model['make']['title'] ?></td>
                    <td><?= $car_model['title'] ?></td>
                    <td><?= $size[$car_model['size']] ?></td>
                    <td><a href="/car-models/edit/<?= $car_model['id'] ?>"><i class="fa fa-edit"></i></a> <a href="/car-models/delete/<?= $car_model['id'] ?>"><i class="fa fa-trash"></i></a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
@endsection
