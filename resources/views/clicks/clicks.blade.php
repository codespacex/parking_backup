@extends('layouts.admin')

@section('content')

    <?php
    ?>
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Clicks</strong>
        </div>
        <div class="card-body">
            <select onchange="changeFunc();" id="select-clicks" class="form-control-lg form-control" style="margin-bottom: 50px">
                <option <?= $sort == 'today' ? 'selected' : '' ?> value="today"><a href="/statistics/today">Today</a></option>
                <option <?= $sort == 'week' ? 'selected' : '' ?>  value="week"><a href="/statistics/week">Last Week</a></option>
                <option <?= $sort == 'month' ? 'selected' : '' ?> value="month"><a href="/statistics/month">Last Month</a></option>
                <option <?= $sort == 'year' ? 'selected' : '' ?> value="year"><a href="/statistics/year">Last Year</a></option>
                <option <?= $sort == 'all' ? 'selected' : '' ?> value="all"><a href="/statistics/all">All time</a></option>
            </select>

            <script>
                function changeFunc() {
                    var selectBox = document.getElementById("select-clicks");
                    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                    window.location.replace("/clicks/"+ selectedValue);
                }
            </script>

            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>State</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Clicked find</td>
                    <td><?= count($find) ?></td>
                </tr>
                <tr>
                    <td>Clicked sell</td>
                    <td><?= count($sell) ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
