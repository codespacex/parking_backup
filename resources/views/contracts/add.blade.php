@extends('layouts.admin')

<?php
    $contracts_array = ['in_process'=>'In Process','finished'=>'Finished'];
    $rates = ['0','1','2','3','4','5'];
?>

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><strong>Edit Contract</strong></div>
                <form method="post" action="/contracts/update" class="card-body card-block">
                    <a href="/contracts"><div class="btn btn-primary">Back</div></a>
                    <input type="hidden" name="id" value="<?= $contract->id ?>">

                    <div class="form-group"><label for="size" class=" form-control-label">Status</label>
                        <select name="status" id="status" class="form-control-lg form-control">
                            <?php foreach ($contracts_array as $key => $value){ ?>
                            <?php if($contract->status == $key){ ?>
                                <option selected value="<?= $key ?>"><?= $value ?></option>
                            <?php }else{ ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group"><label for="size" class=" form-control-label">Rate</label>
                        <select name="rating" id="rating" class="form-control-lg form-control">
                            <?php foreach ($rates as $key => $value){ ?>
                            <?php if($contract->rating == $key){ ?>
                            <option selected value="<?= $key ?>"><?= $value ?></option>
                            <?php }else{ ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group"><label for="size" class=" form-control-label">Feedback</label>
                        <select name="feedback_id" id="feedback_id" class="form-control-lg form-control">
                            <?php foreach ($feedbacks as $key => $value){ ?>
                            <?php if($contract->feedback_id == $value['id']){ ?>
                                <option selected value="<?= $value['id'] ?>"><?= $value['content'] ?></option>
                            <?php }else{ ?>
                                <option value="<?= $value['id'] ?>"><?= $value['content'] ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-header"><strong>Buyer</strong></div>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td><?= !empty($contract->proposal->buyer->name) ? $contract->proposal->buyer->name : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Car</td>
                                    <td><?= !empty($contract->proposal->buyer->car->make->title) ? $contract->proposal->buyer->car->make->title.' '.$contract->proposal->buyer->car->title : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Rating</td>
                                    <td><?= !empty($contract->proposal->buyer->rating) ? $contract->proposal->buyer->rating : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Bought</td>
                                    <td><?= count($buyer['bought']) ?></td>
                                </tr>
                                <tr>
                                    <td>Sold</td>
                                    <td><?= count($buyer['sold']) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-header"><strong>Seller</strong></div>
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <tbody>
                            <tr>
                                <td>Name</td>
                                <td><?= !empty($contract->proposal->seller->name) ? $contract->proposal->seller->name : '' ?></td>
                            </tr>
                            <tr>
                                <td>Car</td>
                                <td><?= !empty($contract->proposal->seller->car->make->title) ? $contract->proposal->seller->car->make->title.' '.$contract->proposal->seller->car->title : '' ?></td>
                            </tr>
                            <tr>
                                <td>Rating</td>
                                <td><?= !empty($contract->proposal->seller->rating) ? $contract->proposal->seller->rating : '' ?></td>
                            </tr>
                            <tr>
                                <td>Bought</td>
                                <td><?= count($seller['bought']) ?></td>
                            </tr>
                            <tr>
                                <td>Sold</td>
                                <td><?= count($seller['sold']) ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection