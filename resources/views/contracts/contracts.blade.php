@extends('layouts.admin')

<?php
    $contracts_array = ['in_process'=>'In Process','finished'=>'Finished'];
    $size = ['1'=>'3.8','2'=>'4.2','3'=>'4.5','4'=>'4.8','5'=>'5.1'];
?>

@section('content')

    <div class="card">
        <div class="card-header">
            <strong class="card-title">Contracts</strong>
        </div>
        <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Start</th>
                    <th>Finish</th>
                    <th>Duration</th>
                    <th>Buyer</th>
                    <th>Seller</th>
                    <th>Status</th>
                    <th>Rate</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($contracts as $contract){ ?>
                <tr>
                    <td><?= $contract['id'] ?></td>
                    <td><?= $contract['start'] ?></td>
                    <td><?= $contract['start'] ?></td>
                    <td><?= !empty($contract['proposal']['minutes']) ? $contract['proposal']['minutes'].' mins' : '' ?></td>
                    <td><?= !empty($contract['proposal']['buyer']) ? '<a target="blank" href="/users/edit/'.$contract['proposal']['buyer']['id'].'">' .$contract['proposal']['buyer']['name']. ' '. $contract['proposal']['buyer']['phone']. ' '. $size[$contract['proposal']['buyer']['car']['size']] . ' ' . $contract['proposal']['buyer']['rating']. '</a>'  : '' ?></td>
                    <td><?= !empty($contract['proposal']['seller']) ? '<a target="blank" href="/users/edit/'.$contract['proposal']['seller']['id'].'">'. $contract['proposal']['seller']['name']. ' '. $contract['proposal']['seller']['phone']. ' '. $size[$contract['proposal']['buyer']['car']['size']] . ' '. $contract['proposal']['seller']['rating']. '</a>'  : '' ?></td>
                    <td><?= $contracts_array[$contract['status']] ?></td>
                    <td><?= !empty($contract['rating'])? $contract['rating'] : '' ?></td>
                    <td><a href="/contracts/edit/<?= $contract['id'] ?>"><i class="fa fa-edit"></i></a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            {{ $contracts->links() }}
        </div>
    </div>
@endsection
