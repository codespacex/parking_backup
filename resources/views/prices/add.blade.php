@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><strong>Edit price</strong></div>
                <form method="post" action="/prices/update" class="card-body card-block">
                    <a href="/prices"><div class="btn btn-primary">Back</div></a>
                    <?php if(!empty($price)){ ?>
                        <input type="hidden" name="id" value="<?= $price->id ?>">
                    <?php } ?>
                    <div class="form-group">
                        <label for="company" class=" form-control-label">Price</label>
                        <input type="text" name="price" id="company" placeholder="Enter title" class="form-control" value="<?= $price->price ?>">
                    </div>
                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection