@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Prices</strong>
        </div>
        <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($prices as $price){ ?>
                <tr>
                    <td><?= $price['key'] ?></td>
                    <td><?= $price['price'] ?></td>
                    <td><a href="/prices/edit/<?= $price['id'] ?>"><i class="fa fa-edit"></i></a> </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection