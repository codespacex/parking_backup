@extends('layouts.admin')

@section('content')

    <?php
        $size = ['1'=>'3.8','2'=>'4.2','3'=>'4.5','4'=>'4.8','5'=>'5.1'];
    ?>
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Statistics</strong>
        </div>
        <div class="card-body">
            <select onchange="changeFunc();" id="select-statistics" class="form-control-lg form-control" style="margin-bottom: 50px">
                <option <?= $sort == 'today' ? 'selected' : '' ?> value="today"><a href="/statistics/today">Today</a></option>
                <option <?= $sort == 'week' ? 'selected' : '' ?>  value="week"><a href="/statistics/week">Last Week</a></option>
                <option <?= $sort == 'month' ? 'selected' : '' ?> value="month"><a href="/statistics/month">Last Month</a></option>
                <option <?= $sort == 'year' ? 'selected' : '' ?> value="year"><a href="/statistics/year">Last Year</a></option>
                <option <?= $sort == 'all' ? 'selected' : '' ?> value="all"><a href="/statistics/all">All time</a></option>
            </select>

            <script>
                function changeFunc() {
                    var selectBox = document.getElementById("select-statistics");
                    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                    window.location.replace("/statistics/"+ selectedValue);
                }
            </script>

            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>All order count</td>
                        <td><?= count($contracts) ?></td>
                    </tr>
                    <tr>
                        <td>Users rated 5</td>
                        <td><?= count($contracts_five) ?></td>
                    </tr>
                    <tr>
                        <td>Users rated 1-4</td>
                        <td><?= count($contracts_other_rate) ?></td>
                    </tr>
                    <tr>
                        <td>Users not rated</td>
                        <td><?= count($contracts_not_rated) ?></td>
                    </tr>
                    <tr>
                        <td>The driver was not in place</td>
                        <td><?= count($contracts_not_in_the_place) ?></td>
                    </tr>
                    <tr>
                        <td>The driver was in wrong place</td>
                        <td><?= count($contracts_driver_not_in_the_place) ?></td>
                    </tr>
                    <tr>
                        <td>The time was not enough</td>
                        <td><?= count($contracts_time_not_enough) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
