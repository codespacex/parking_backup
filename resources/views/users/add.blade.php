@extends('layouts.admin')

@section('content')
    <?php
        $contracts_array = ['in_process'=>'In Process','finished'=>'Finished'];
    ?>

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header"><strong>Edit User</strong></div>
                <form method="post" action="/users/update" class="card-body card-block">
                    <a href="/home"><div class="btn btn-primary">Back</div></a>
                    <?php if(!empty($user)){ ?>
                        <input type="hidden" name="id" value="<?= $user->id ?>">
                    <?php } ?>
                    <div class="form-group">
                        <label for="company" class=" form-control-label">Name</label>
                        <input type="text" name="name" id="name" placeholder="Enter title" required class="form-control" value="<?= $user->name ?>">
                    </div>
                    <div class="form-group">
                        <label for="company" class=" form-control-label">Phone</label>
                        <input type="text" name="phone" id="phone" placeholder="Enter title" required class="form-control" value="<?= $user->phone ?>">
                    </div>
                    <div class="form-group">
                        <label for="company" class=" form-control-label">Car number</label>
                        <input type="text" name="car_number" id="car_number" placeholder="Enter title" required class="form-control" value="<?= $user->car_number ?>">
                    </div>
                    <div class="form-group"><label for="car_make_id" class=" form-control-label">Car Make</label>
                        <select name="car_make_id" id="car_make_id" class="form-control-lg form-control">
                            <?php foreach ($car_makes as $car_make){ ?>
                            <?php if($user->car->make->id == $car_make['id']){ ?>
                                <option selected value="<?= $car_make['id'] ?>"><?= $car_make['title'] ?></option>
                            <?php }else{ ?>
                                <option value="<?= $car_make['id'] ?>"><?= $car_make['title'] ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group"><label for="car_make_id" class=" form-control-label">Car Model</label>
                        <select name="car_id" id="car_id" class="form-control-lg form-control">
                            <?php foreach ($models as $car_model){ ?>
                            <?php if($user->car->id == $car_model['id']){ ?>
                            <option selected value="<?= $car_model['id'] ?>"><?= $car_model['title'] ?></option>
                            <?php }else{ ?>
                            <option value="<?= $car_model['id'] ?>"><?= $car_model['title'] ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="company" class=" form-control-label">Rating</label>
                        <p style="color:red"><?= $user->rating ?></p>
                    </div>

                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button>
                </form>

                <div class="card-body">
                    <div class="card-header"><strong>Bought</strong></div>
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Duration</th>
                            <th>Buyer</th>
                            <th>Seller</th>
                            <th>Status</th>
                            <th>Rate</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($contracts['bought'] as $contract){ ?>
                        <tr>
                            <td><?= $contract['id'] ?></td>
                            <td><?= $contract['start'] ?></td>
                            <td><?= $contract['finish'] ?></td>
                            <td><?= !empty($contract['proposal']['minutes']) ? $contract['proposal']['minutes'].' mins' : '' ?></td>
                            <td><?= !empty($contract['proposal']['buyer']) ? $contract['proposal']['buyer']['name']. ' '. $contract['proposal']['buyer']['car_number']. ' '. $contract['proposal']['buyer']['rating']  : '' ?></td>
                            <td><?= !empty($contract['proposal']['seller']) ? $contract['proposal']['seller']['name']. ' '. $contract['proposal']['seller']['car_number']. ' '. $contract['proposal']['seller']['rating']  : '' ?></td>
                            <td><?= $contracts_array[$contract['status']] ?></td>
                            <td><?= !empty($contract['rating'])? $contract['rating'] : '' ?></td>
                            <td><a href="/contracts/edit/<?= $contract['id'] ?>"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="card-body">
                    <div class="card-header"><strong>Sold</strong></div>
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Duration</th>
                            <th>Buyer</th>
                            <th>Seller</th>
                            <th>Status</th>
                            <th>Rate</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($contracts['sold'] as $contract){ ?>
                        <tr>
                            <td><?= $contract['id'] ?></td>
                            <td><?= $contract['start'] ?></td>
                            <td><?= $contract['finish'] ?></td>
                            <td><?= !empty($contract['proposal']['minutes']) ? $contract['proposal']['minutes'].' mins' : '' ?></td>
                            <td><?= !empty($contract['proposal']['buyer']) ? $contract['proposal']['buyer']['name']. ' '. $contract['proposal']['buyer']['car_number']. ' '. $contract['proposal']['buyer']['rating']  : '' ?></td>
                            <td><?= !empty($contract['proposal']['seller']) ? $contract['proposal']['seller']['name']. ' '. $contract['proposal']['seller']['car_number']. ' '. $contract['proposal']['seller']['rating']  : '' ?></td>
                            <td><?= $contracts_array[$contract['status']] ?></td>
                            <td><?= !empty($contract['rating'])? $contract['rating'] : '' ?></td>
                            <td><a href="/contracts/edit/<?= $contract['id'] ?>"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection