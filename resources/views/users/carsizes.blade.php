@extends('layouts.admin')

@section('content')

    <?php
    ?>
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Car Sizes</strong>
            <h6>Overall count <?= count($users) ?></h6>
        </div>
        <div class="card-body">

            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Size</th>
                    <th>Count</th>
                    <th>Percent</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cars as $car){ ?>
                <tr>
                    <td><?= $car['size'] ?></td>
                    <td><?= $car['count'] ?></td>
                    <td><?= (float)sprintf('%.1f', $car['percent']).' %' ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
