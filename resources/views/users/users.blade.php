@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Users</strong>
        </div>
        <div class="card-body">
            <div class="search">
                <form method="post" action="/users/find" class="card-body card-block">
                    <?= !empty($word) ? 'Found '.count($users). ' results for <strong>'.$word.'.</strong>': ''  ?>
                    <div  class="form-group" >
                        <input value="<?= !empty($word) ? $word : ''  ?>" style="display: inline-block; max-width: 300px" class="form-control" type="text" name="number" placeholder="Type number">
                        {{ csrf_field() }}
                        <button style="display:inline-block;margin-bottom: 5px;" class="btn btn-success" type="submit">Find</button>
                    </div>
                </form>
            </div>
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Car Number</th>
                        <th>Phone</th>
                        <th>Car</th>
                        <th>Rating</th>
                        <th>Is Confirmed</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($users as $user){ ?>
                    <tr>
                        <td><?= $user['name'] ?></td>
                        <td><?= $user['car_number'] ?></td>
                        <td><?= $user['phone'] ?></td>
                        <td><?= $user['car']['make']['title'].' '.$user['car']['title'] ?></td>
                        <td><?= $user['rating'] ?></td>
                        <?php if($user['is_confirmed'] == '1'){ ?>
                            <td style="color:green">Yes</td>
                        <?php }else{ ?>
                            <td style="color:red">No</td>
                        <?php } ?>
                        <td><a href="/users/edit/<?= $user['id'] ?>"><i class="fa fa-edit"></i></a></td>
                        <?php if($user['is_blocked']){ ?>
                            <td><a class="btn btn-success" href="/users/unblock/<?= $user['id'] ?>">Unblock</a></td>
                        <?php }else{ ?>
                            <td><a class="btn btn-danger" href="/users/block/<?= $user['id'] ?>">Block</a></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
