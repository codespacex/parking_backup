<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
        <script>
            var socket = io('http://167.99.41.137:3000?token=4cb97f2d314f99e0d6c3bbb5a61103bea9c56958d5b17b5cf28c4344fa37ebbe');
//            var socket = io('http://127.0.0.1:3000?token=user1');

            socket.on('Emit' , function(message){
                console.log(message);
            });

            socket.on('Proposals' , function(message){
                console.log('unavailable',message);
            });

            socket.on('ProposalCanceled' , function(message){
                console.log('ProposalCanceled' , message);
            });

            socket.on('ProposalDeclined' , function(message){
                console.log('ProposalDeclined' , message);
            });

            socket.on('ProposalAccepted' , function(message){
                console.log('ProposalAccepted' ,message);
            });

            socket.on('ProposalFinished' , function(message){
                console.log('ProposalFinished' ,message);
            });

            socket.on('Locations' , function(message){
                console.log('Locations' , message);
            });

            socket.on('SendLocation' , function(message){
                console.log('Location Sent' , message);
            });



        </script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                    {{--<script src="//localhost:6001/socket.io/socket.io.js"></script>--}}
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
event(new \App\Events\Emit(['data' => 'msg' , 'token' => []]));
//event(new \App\Events\RequestAvailableStarted('sd'));
