<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::routes();

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('Emit', function () {
    return true;
});

Broadcast::channel('Available', function () {
    return true;
});

Broadcast::channel('Notavailable', function () {
    return true;
});

Broadcast::channel('Proposals', function () {
    return true;
});

Broadcast::channel('Locations', function () {
    return true;
});