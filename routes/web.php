<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'UserController@index')->name('home');
Route::get('/users/edit/{id}', 'UserController@edit')->name('edit-user');
Route::post('/users/update', 'UserController@update')->name('update-user');
Route::get('/users/car-sizes', 'UserController@carSizes')->name('car-sizes');
Route::post('/users/find', 'UserController@find')->name('find-user');
Route::get('/users/block/{id}', 'UserController@block')->name('block-user');
Route::get('/users/unblock/{id}', 'UserController@unblock')->name('unblock-user');

Route::get('/car-makes', 'CarMakesController@index')->name('car-makes');
Route::get('/car-makes/add', 'CarMakesController@add')->name('add-car-make');
Route::get('/car-makes/edit/{id}', 'CarMakesController@edit')->name('edit-car-make');
Route::get('/car-makes/delete/{id}', 'CarMakesController@delete')->name('delete-car-make');

Route::post('/car-makes/create', 'CarMakesController@create')->name('create-car-make');
Route::post('/car-makes/update', 'CarMakesController@update')->name('update-car-make');

Route::get('/car-models', 'CarModelsController@index')->name('car-models');
Route::get('/car-models/add', 'CarModelsController@add')->name('add-car-model');
Route::get('/car-models/edit/{id}', 'CarModelsController@edit')->name('edit-car-model');
Route::get('/car-models/delete/{id}', 'CarModelsController@delete')->name('delete-car-model');

Route::post('/car-models/create', 'CarModelsController@create')->name('create-car-model');
Route::post('/car-models/update', 'CarModelsController@update')->name('update-car-model');

Route::get('/contracts', 'ContractsController@index')->name('contracts');
Route::get('/contracts/edit/{id}', 'ContractsController@edit')->name('edit-contracts');

Route::post('/contracts/update', 'ContractsController@update')->name('update-contracts');

Route::get('/statistics/{sort}', 'StatisticsController@index')->name('statistics');
Route::get('/clicks/{sort}', 'ClicksController@index')->name('clicks');

Route::get('/prices', 'PricesController@index')->name('prices');
Route::get('/prices/edit/{id}', 'PricesController@edit')->name('edit-prices');
Route::post('/prices/update', 'PricesController@update')->name('update-price');
