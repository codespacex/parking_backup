var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
var request = require('request');

redis.subscribe('Emit', function(err, count) {});
redis.subscribe('Locations', function(err, count) {});
redis.subscribe('Proposals', function(err, count) {});
redis.subscribe('ProposalCanceled', function(err, count) {});
redis.subscribe('ProposalAccepted', function(err, count) {});
redis.subscribe('ProposalDeclined', function(err, count) {});
redis.subscribe('ProposalFinished', function(err, count) {});
redis.subscribe('Available', function(err, count) {});
redis.subscribe('Notavailable', function(err, count) {});

redis.on('message', function(channel, message) {
    
    if (channel == 'SendLocation'){
        request.post(
            'http://167.99.41.137/api/general/send-location',
            { json: { lng: '40.2847800',lat: '44.4891500',contract_id : '254' , user_id : '3' } },
            function (error, response, body) {
                console.log(response)
                if (!error && response.statusCode == 200) {
                    console.log(body)
                }
            }
        );

    }
    // console.log('Message Recieved: ' + message);
    message = JSON.parse(message);

    var rooms = Object.keys(io.sockets.sockets);
    var tokens = message.data.data.token;

    if (tokens.length == 0){
        io.emit(channel, message.data.data.data);
    }

    var array = [];
    var roomsToPush = [];

    for (var i = 0 ; i < rooms.length ; i++){
        var token = io.sockets.sockets[rooms[i]].handshake.query.token;
        var tmp = {
            'user' : token,
            'room' : rooms[i]
        }
        array.push(tmp);
    }

    for (var j = 0 ; j < tokens.length ; j++){
        var token = tokens[j];

        for (var k=0 ; k < array.length ; k++){
            var data = array[k];

            if (token == data.user && !roomsToPush.includes(data.room)){
                roomsToPush.push(data.room);
                io.to(data.room).emit(channel, message.data.data.data);
            }
        }
    }

});


http.listen(3000, function(){
    console.log('Listening on Port 3000');
});